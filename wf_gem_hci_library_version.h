//  Copyright (c) 2012-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//


#ifndef _WF_GEM_HCI_LIBRARY_VERSION_H_
#define _WF_GEM_HCI_LIBRARY_VERSION_H_


#define GEM_HCI_LIBRARY_VERSION_MAJOR       1
#define GEM_HCI_LIBRARY_VERSION_MINOR       1
#define GEM_HCI_LIBRARY_VERSION_BUILD       0


// v1.1.0 2019-03-14 Aaron Fontaine
// ----------------------------------------------------------------------------------------------
// + GEM-293 - Enforce throttling delays during image transfer


#endif /* _WF_GEM_HCI_LIBRARY_VERSION_H_ */

//  Copyright (c) 2012-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

 
#include "wf_gem_hci_comms.h"

#include <string.h>


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Macros and Definitions										|
// |																								|
// +------------------------------------------------------------------------------------------------+

// the doubling of everything between STX and ETX accounts for the fact that these bytes might need to be excaped with DLEs
//										STX + (event|msg_class + msg_id + data_len + data + checksum) * 2 + ETX
#define WF_GEM_HCI_COMMS_FRAME_LEN_MAX              (1 + (1 + 1 + 1 + WF_GEM_HCI_COMMS_MESSAGE_DATA_LEN_MAX + 1) * 2 + 1)


#define WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_STX     0x02
#define WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_ETX     0x03
#define WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_DLE     0x10

// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Public Variables											|
// |																								|
// +------------------------------------------------------------------------------------------------+


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Private Variables											|
// |																								|
// +------------------------------------------------------------------------------------------------+
static struct rx_frame_buffer_t
{
    bool		started;
    uint8_t 	buffer[WF_GEM_HCI_COMMS_FRAME_LEN_MAX];
    uint16_t 	buffer_offset;
    bool 		next_rx_byte_is_escaped;
    uint8_t 	running_checksum;
}
_incoming_frame;



// +------------------------------------------------------------------------------------------------+
// |																								|
// |							Private Function Declarations										|
// |																								|
// +------------------------------------------------------------------------------------------------+
static void handle_uart_rx(uint8_t rx_byte, bool error);


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Public Functions											|
// |																								|
// +------------------------------------------------------------------------------------------------+

void wf_gem_hci_comms_init(void)
{

}

void wf_gem_hci_comms_start(void)
{
    memset(&_incoming_frame, 0, sizeof(_incoming_frame));
}

void wf_gem_hci_comms_stop(void)
{

}



void wf_gem_hci_comms_send_message(wf_gem_hci_comms_message_t* message)
{
    if (message == NULL)
    {
        return;
    }

    if (message->data_length > WF_GEM_HCI_COMMS_MESSAGE_DATA_LEN_MAX)
    {
        // can not send a message this large!
        return;
    }

    uint8_t frame_buf[WF_GEM_HCI_COMMS_FRAME_LEN_MAX];
    uint16_t frame_length = 0;
    frame_buf[frame_length++] = ( message->message_event_flag ? 0x80 : 0x00 ) | (0x7F & message->message_class_id);
    frame_buf[frame_length++] = message->message_id;
    frame_buf[frame_length++] = message->data_length;
    memcpy(&frame_buf[frame_length], message->data, message->data_length);
    frame_length += message->data_length;

    // compute checksum
    uint8_t checksum = 0;
    for (uint16_t i = 0; i < frame_length; i++)
    {
        uint8_t frame_byte = frame_buf[i];
        checksum = checksum ^ frame_byte;
    }
    frame_buf[frame_length++] = checksum;

    wf_gem_hci_comms_on_send_byte(WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_STX);
    for (uint16_t i = 0; i < frame_length; i++)
    {
        uint8_t frame_byte = frame_buf[i];
        if (frame_byte == WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_STX ||
            frame_byte == WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_ETX ||
            frame_byte == WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_DLE)
        {
            wf_gem_hci_comms_on_send_byte(WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_DLE);
        }
        wf_gem_hci_comms_on_send_byte(frame_byte);
    }
    wf_gem_hci_comms_on_send_byte(WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_ETX);
}


void wf_gem_hci_comms_process_rx_byte(uint8_t rx_byte)
{
    handle_uart_rx(rx_byte, false);
}

void wf_gem_hci_comms_handle_rx_error(void)
{
    handle_uart_rx(0, true);
}


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Private Functions											|
// |																								|
// +------------------------------------------------------------------------------------------------+


static void finalise_rx_frame(void)
{
    if (_incoming_frame.buffer_offset == 0)
    {
        // empty frame
        return;
    }

    if (_incoming_frame.running_checksum != 0)
    {
        // checksum is invalid
        return;
    }

    // "remove" the checksum from the buffer offset
    _incoming_frame.buffer_offset--;

    // the checksum looks valid
    // perform a length check on the frame now
    if (_incoming_frame.buffer_offset < 3)
    {
        // invalid packet structure
        return;
    }

    uint8_t data_length = _incoming_frame.buffer[2];
    if (data_length > WF_GEM_HCI_COMMS_MESSAGE_DATA_LEN_MAX)
    {
        return;
    }

    if (_incoming_frame.buffer_offset - 3 != data_length)
    {
        // invalid packet structure / data length
        return;
    }

    bool message_event_flag = _incoming_frame.buffer[0] & 0x80;
    uint8_t message_class_id = _incoming_frame.buffer[0] & 0x7F;
    uint8_t message_id = _incoming_frame.buffer[1];

    wf_gem_hci_comms_message_t message;

    message.message_event_flag = message_event_flag;
    message.message_class_id = message_class_id;
    message.message_id = message_id;
    message.data_length = data_length;
    memcpy(message.data, &_incoming_frame.buffer[3], data_length);

    wf_gem_hci_comms_on_message_received(&message);
}

// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Event Handlers												|
// |																								|
// +------------------------------------------------------------------------------------------------+

static void handle_uart_rx(uint8_t rx_byte, bool error)
{
    if (error)
    {
        // this callback represents a UART RX error only - not an incoming byte
        // flag the error

        // discard the frame currently being unpacked (if one is)
        _incoming_frame.started = false;
        return;
    }

    bool rx_byte_was_escaped = false;

    if (_incoming_frame.next_rx_byte_is_escaped)
    {
        if (rx_byte == WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_STX ||
            rx_byte == WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_ETX ||
            rx_byte == WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_DLE)
        {
            _incoming_frame.next_rx_byte_is_escaped = false;
            rx_byte_was_escaped = true;
        }
        else
        {
            // this is invalid - we expected a control character after the escape char
            _incoming_frame.started = false;
            return;
        }
    }
    else if (rx_byte == WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_DLE)
    {
        _incoming_frame.next_rx_byte_is_escaped = true;
        return;
    }

    if (!rx_byte_was_escaped && rx_byte == WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_ETX)
    {
        if (_incoming_frame.started)
        {
            // finalise the frame
            finalise_rx_frame();

            _incoming_frame.started = false;
        }
        else
        {
            // ignore this ETX - we haven't started a frame now
        }
    }
    else if (!rx_byte_was_escaped && rx_byte == WF_GEM_HCI_COMMS_FRAME_CONTROL_CHAR_STX)
    {
        // setup for new frame RX
        memset(&_incoming_frame, 0, sizeof(_incoming_frame));
        _incoming_frame.started = true;
    }
    else if (_incoming_frame.started)
    {
        if (_incoming_frame.buffer_offset >= WF_GEM_HCI_COMMS_FRAME_LEN_MAX)
        {
            // the rx buffer has overflowed now
            _incoming_frame.started = false;
        }
        else
        {
            // add the byte to the rx buffer
            _incoming_frame.buffer[_incoming_frame.buffer_offset] = rx_byte;
            _incoming_frame.buffer_offset++;
            _incoming_frame.running_checksum = _incoming_frame.running_checksum ^ rx_byte;
        }
    }
    else
    {
        // frame rx has not started yet.
    }
}

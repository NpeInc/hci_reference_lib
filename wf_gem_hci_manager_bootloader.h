//  Copyright (c) 2012-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//


#ifndef _WF_GEM_HCI_MANAGER_BOOTLOADER_H_
#define _WF_GEM_HCI_MANAGER_BOOTLOADER_H_


#include "wf_gem_hci_config.h"
#if WF_GEM_HCI_CONFIG_INCLUDE_BOOTLOADER_MANAGER


#include "wf_gem_hci_manager.h"



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Public Defines, Constants, Types
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
typedef enum
{
    WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_STARTING_BOOTLOADER,
    WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_STARTING_UPDATE,
    WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_SENDING_UPDATE_DATA,
    WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_FINISHING_UPDATE,
    WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_UPDATE_COMPLETE,
    
    WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_UPDATE_FAILED,
}
wf_gem_hci_manager_bootloader_updater_state_e;

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Public Variables
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Public functions
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+


// Please note that the final 2 bytes of image data must be the image data CRC. Firmware update .bin files provided by Wahoo Fitness 
// will already contain the image CRC as the final 2 bytes of the binary file.
void wf_gem_hci_manager_bootloader_begin_update_process(uint16_t start_bootloader_timeout_ms, uint32_t update_image_size);


// This can be used at any time to exit the bootloader.
void wf_gem_hci_manager_bootloader_cancel_update_process(bool exit_bootloader);


// Only call this method when a wf_gem_hci_manager_bootloader_on_update_image_data_required(...) callback
// has been received.
// Note: the provided data buffer will be copied to internal memory, so it does not need to remain
// valid once this function returns
// It is recommended that this method not be called from an interrupt service routine (interrupt context)
void wf_gem_hci_manager_bootloader_send_update_image_data(const uint8_t* const data, uint8_t num_data_bytes);


// it is recommended that this method not be called from an interrupt service routine (interrupt context)
// For example - the timer event should be "scheduled" on the next main loop processing so that this method is
// called from the main loop, and not from an interrupt context.
void wf_gem_hci_manager_bootloader_handle_update_process_timer_fired(void);


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		External Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+



// When the host application receives this event, it must then later make a call to wf_gem_hci_manager_bootloader_send_update_image_data(...)
// For example, the host might fetch the update image data bytes from an attached USB mass storage device and copy the bytes to a RAM buffer,
// the RAM buffer can then be passed into the wf_gem_hci_manager_bootloader_send_update_image_data(...) function.
//
// Notes: 
//   - It is permitted for the wf_gem_hci_manager_bootloader_on_update_image_data_required handler to call 
//     wf_gem_hci_manager_bootloader_send_update_image_data directly (no delay, no scheduling on the next main loop iteration etc.).
//
//   - The final 2 bytes of image data must be the image data CRC. Firmware update .bin files provided by Wahoo Fitness 
//     will already contain the image CRC as the final 2 bytes of the binary file.
//
// Parameters:
// reset_index 
//    - if this is true, the host application must reset the data index, the next call to wf_gem_hci_manager_bootloader_send_update_image_data
//      must contain the bytes from the start of the image, 
//      e.g. byte[0] to byte[maximum_data_bytes - 1]
//      if this is false, the host application must send subsequent image data to wf_gem_hci_manager_bootloader_send_update_image_data
//      e.g. byte[n] to byte[n + (maximum_data_bytes - 1)] where n is the number of bytes that have already been passed to the 
//      wf_gem_hci_manager_bootloader_send_update_image_data function
//
// maximum_data_bytes 
//    - the host application must not pass more than maximum_data_bytes worth of data in each call to
//      wf_gem_hci_manager_bootloader_send_update_image_data
extern void wf_gem_hci_manager_bootloader_on_update_image_data_required(bool reset_index, uint8_t maximum_data_bytes);




// This callback is used when the update process needs a timer to perform an internal function.
// When the timer has expire/finished, the application must call the wf_gem_hci_manager_bootloader_handle_update_process_timer_fired() method.
// The timer must only fire once for each begin call; the timer must not be a repeating timer (it should be "single shot").
// If the cmd_timeout_ms value is 0, the application should call wf_gem_hci_manager_bootloader_handle_update_process_timer_fired() as soon 
// as possible (with minimal delay), for example on the next iteration of the "main loop".  
// 
// wf_gem_hci_manager_bootloader_handle_update_process_timer_fired() must not be called directly from the 
// wf_gem_hci_manager_bootloader_on_begin_update_process_timer(...) handler implementation.
extern void wf_gem_hci_manager_bootloader_on_begin_update_process_timer(uint16_t cmd_timeout_ms);


// cancel/stop the timer started in wf_gem_hci_manager_bootloader_on_begin_update_process_timer(...) (if it is running)
extern void wf_gem_hci_manager_bootloader_on_cancel_update_process_timer(void);


// This method is called when the update progress (state) changes.
//
// If a failure occurs, updater_state will equal WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_UPDATE_FAILED.  The host application 
// can attempt another update by calling wf_gem_hci_manager_bootloader_begin_update_process(...), or the host application 
// can instruct the bootloader to exit by calling wf_gem_hci_manager_bootloader_cancel_update_process(true).
//
// state_progress_percent is only valid when the updater_state is equal to WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_SENDING_UPDATE_DATA
extern void wf_gem_hci_manager_bootloader_on_update_progress_changed(wf_gem_hci_manager_bootloader_updater_state_e updater_state, uint8_t state_progress_percent);




#endif // #if WF_GEM_HCI_CONFIG_INCLUDE_BOOTLOADER_MANAGER

#endif

//  Copyright (c) 2012-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#ifndef _WF_GEM_HCI_MANAGER_H_
#define _WF_GEM_HCI_MANAGER_H_



#include "wf_gem_hci_comms.h"

// Timeout and retry defines.
#define WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS   1
#define WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS           50
#define WF_GEM_HCI_LONG_COMMAND_TIMEOUT_MS              200 // Timeout used for operations that require a flash write

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// |																			|
// |		Public Defines, Constants, Types									|
// |																			|
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

#define BT_ADDRESS_SIZE                         6
#define BT_MAX_LOCAL_NAME                       16
#define MAX_NFC_READ_PAYLOAD                    ((uint8_t) 128 - 4) // MAX HCI data length - 4 for other data overhead
#define MAX_STANDARD_LONG_UUID_SIZE             16

typedef char utf8_data_t;

typedef enum
{
    WF_GEM_HCI_MSG_CLASS_BOOTLOADER             = 0x00,

    WF_GEM_HCI_MSG_CLASS_SYSTEM                 = 0x01,
    WF_GEM_HCI_MSG_CLASS_HARDWARE               = 0x02,
    WF_GEM_HCI_MSG_CLASS_BT_CONTROL             = 0x10,
    WF_GEM_HCI_MSG_CLASS_BT_CONFIG              = 0x11,
    WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO         = 0x12,
    WF_GEM_HCI_MSG_CLASS_BT_RECEIVER            = 0x13,
    WF_GEM_HCI_MSG_CLASS_ANT_CONTROL            = 0x20,
    WF_GEM_HCI_MSG_CLASS_ANT_CONFIG             = 0x21,
    WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER           = 0x22,
    WF_GEM_HCI_MSG_CLASS_NFC_READER             = 0x40,
    WF_GEM_HCI_MSG_CLASS_GYM_CONNECT            = 0x60
}
wf_gem_hci_msg_class_e;


typedef enum
{
    WF_GEM_HCI_COMMAND_ID_SYSTEM_PING                           = 0x01,
    WF_GEM_HCI_COMMAND_ID_SYSTEM_SHUTDOWN                       = 0x02,
    WF_GEM_HCI_COMMAND_ID_SYSTEM_GET_GEM_MODULE_VERSION_INFO    = 0x03,
    WF_GEM_HCI_COMMAND_ID_SYSTEM_RESET                          = 0x04,
    WF_GEM_HCI_COMMAND_ID_SYSTEM_DISABLE_HCI_INTERFACE          = 0x20,
    WF_GEM_HCI_COMMAND_ID_SYSTEM_START_RADIO_TEST               = 0xE0,
    WF_GEM_HCI_COMMAND_ID_SYSTEM_STOP_RADIO_TEST                = 0xE1,
    WF_GEM_HCI_COMMAND_ID_SYSTEM_INITIATE_BOOTLOADER            = 0xF0,
}
wf_gem_hci_command_id_system_e;

typedef enum
{
    WF_GEM_HCI_EVENT_ID_SYSTEM_POWER_UP                 = 0x01,
    WF_GEM_HCI_EVENT_ID_SYSTEM_SHUTDOWN                 = 0x02,
    WF_GEM_HCI_EVENT_ID_SYSTEM_BOOTLOADER_INITIATED     = 0x03,

}
wf_gem_hci_event_id_system_e;


typedef enum
{
    WF_GEM_HCI_COMMAND_ID_HARDWARE_GET_PIN_IO_CONFIG    = 0x01,
    WF_GEM_HCI_COMMAND_ID_HARDWARE_SET_PIN_IO_CONFIG    = 0x02,
}
wf_gem_hci_command_id_hardware_e;




// +--------------------------------------------------------------------------+
// |                                                                          |
// |                          HCI_MSG_CLASS_BT_CONTORL                        |
// |                                                                          |
// +--------------------------------------------------------------------------+


///
/// Bluetooth Class Commands
///
typedef enum
{
    WF_GEM_HCI_COMMAND_ID_BT_CONTROL_START_ADV              = 0x01,
    WF_GEM_HCI_COMMAND_ID_BT_CONTROL_STOP_ADV               = 0x02,
    WF_GEM_HCI_COMMAND_ID_BT_CONTROL_GET_STATE              = 0x03,
    WF_GEM_HCI_COMMAND_ID_BT_CONTROL_DISCONNECT_CENTRAL     = 0x04,
}
wf_gem_hci_command_id_bt_control_e;

typedef enum
{
    WF_GEM_HCI_EVENT_ID_BT_CONTROL_ADV_TIMEOUT              = 0x01,
    WF_GEM_HCI_EVENT_ID_BT_CONTROL_CENTRAL_CONNECTED_V1     = 0x02,
    WF_GEM_HCI_EVENT_ID_BT_CONTROL_CENTRAL_DISCONNECTED     = 0x03,
    WF_GEM_HCI_EVENT_ID_BT_CONTROL_CENTRAL_CONNECTED        = 0x04,
}
wf_gem_hci_event_id_bt_control_e;


// +--------------------------------------------------------------------------+
// |                                                                          |
// |                          HCI_MSG_CLASS_BT_CONFIG                         |
// |                                                                          |
// +--------------------------------------------------------------------------+

///
/// Bluetooth Config Class Commands
///
typedef enum
{
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_DEVICE_NAME             = 0x01,
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_DEVICE_NAME             = 0x02,
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_ADV_TIMING              = 0x03,
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_ADV_TIMING              = 0x04,
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_DEVICE_ADDR			    = 0x05,
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_DEVICE_ADDR             = 0x06,
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_RADIO_TX_POWER          = 0x07,
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_RADIO_TX_POWER          = 0x08,
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_CONNECTION_INTERVAL     = 0x09,
    WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_CONNECTION_INTERVAL     = 0x0A,
}
wf_gem_hci_command_id_bt_config_e;

//typedef enum
//{
//}
//wf_gem_hci_event_id_bt_config_e;


// +--------------------------------------------------------------------------+
// |                                                                          |
// |                          HCI_MSG_CLASS_BT_DEVICE_INFO                    |
// |                                                                          |
// +--------------------------------------------------------------------------+

///
/// Bluetooth Device Class Commands
///
typedef enum
{
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_MANU_NAME              = 0x01,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MANU_NAME              = 0x02,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_MODEL_NUM              = 0x03,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MODEL_NUM              = 0x04,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_SERIAL_NUM             = 0x05,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_SERIAL_NUM             = 0x06,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_HW_REV                 = 0x07,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_HW_REV                 = 0x08,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_FW_REV                 = 0x09,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_FW_REV                 = 0x0A,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_BATT_SERV_INC          = 0x0B,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BATT_SERV_INC          = 0x0C,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_REPORTED_BATT_LVL      = 0x0D,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_REPORTED_BATT_LVL      = 0x0E,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_PNP_ID                 = 0x0F,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_PNP_ID                 = 0x10,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_BSCS_SERVICE_ENABLED   = 0x11,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BSCS_SERVICE_ENABLED   = 0x12,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_POWER_SERVICE_ENABLED  = 0x13,
    WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_POWER_SERVICE_ENABLED  = 0x14,
}
wf_gem_hci_command_id_bt_device_info_e;

//typedef enum
//{
//}
//wf_gem_hci_event_id_bt_config_e;


// +--------------------------------------------------------------------------+
// |                                                                          |
// |                          HCI_MSG_CLASS_BT_RECEIVER                       |
// |                                                                          |
// +--------------------------------------------------------------------------+

typedef enum
{
    WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_START_DISCOVERY           = 0x01,
    WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_STOP_DISCOVERY            = 0x02,
    WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_CONNECT_DEVICE            = 0x03,
    WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_DISCONNECT_DEVICE         = 0x04,
    WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_START_DISCOVERY_IBEACON   = 0x05
}
wf_gem_hci_command_id_ble_receiver_e;


typedef enum
{
    WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DISCOVERY_TIMEOUT                           = 0x02,
    WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DEVICE_CONNECTED                            = 0x03,
    WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECT_FAIL                                = 0x04,
    WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DEVICE_DISCONNECTED                         = 0x05,
    WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DEVICE_DISCOVERED                           = 0x06,
    WF_GEM_HCI_EVENT_ID_BT_RECEIVER_IBEACON_DISCOVERED                          = 0x07,
    WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECTED_DEVICE_SERIAL_NUMBER_UPDATED      = 0x20,
    WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECTED_DEVICE_BATTERY_STATUS_UPDATED     = 0x21,
    WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECTED_HEART_RATE_MONITOR_DATA           = 0x40,
}
wf_gem_hci_event_id_ble_receiver_e;


// +--------------------------------------------------------------------------+
// |                                                                          |
// |                          HCI_MSG_CLASS_ANT_CONROL                        |
// |                                                                          |
// +--------------------------------------------------------------------------+

typedef enum
{
    WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_ENABLED      = 0x01,
    WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_ENABLED      = 0x02,
    WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_FREQ_DIV     = 0x03,
    WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_FREQ_DIV     = 0x04,
}
wf_gem_hci_command_id_ant_control_e;

//typedef enum
//{
//}
//wf_gem_hci_event_id_ant_control_e;


// +--------------------------------------------------------------------------+
// |                                                                          |
// |                          HCI_MSG_CLASS_ANT_CONFIG                        |
// |                                                                          |
// +--------------------------------------------------------------------------+

typedef enum
{
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_ANT_DEVICE_NUMBER          = 0x01,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_ANT_DEVICE_NUMBER          = 0x02,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_RADIO_TX_POWER             = 0x03,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_RADIO_TX_POWER             = 0x04,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_REPORTED_BATT_LVL          = 0x05,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_REPORTED_BATT_LVL          = 0x06,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_HW_VER                     = 0x07,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_HW_VER                     = 0x08,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_MANU_ID                    = 0x09,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MANU_ID                    = 0x0A,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_MODEL_NUM                  = 0x0B,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MODEL_NUM                  = 0x0C,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_SW_VER                     = 0x0D,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SW_VER                     = 0x0E,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_SERIAL_NUMBER              = 0x0F,
    WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SERIAL_NUMBER              = 0x10,
}
wf_gem_hci_command_id_ant_config_e;

//typedef enum
//{
//}
//wf_gem_hci_event_id_ant_config_e;



// +--------------------------------------------------------------------------+
// |                                                                          |
// |                          HCI_MSG_CLASS_ANT_RECEIVER                      |
// |                                                                          |
// +--------------------------------------------------------------------------+

typedef enum
{
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_START_DISCOVERY                  = 0x01,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_STOP_DISCOVERY                   = 0x02,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_CONNECT_DEVICE                   = 0x03,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_DISCONNECT_DEVICE                = 0x04,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_REQUEST_CALIBRATION              = 0x05,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_MANUFACTURER_INFO            = 0x06,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_BATTERY_STATUS               = 0x07,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_BIKE_POWER_CTF_SAVE_SLOPE        = 0x08,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_BIKE_POWER_CTF_SAVE_SERIAL       = 0x09,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_ENABLE_FREQUENCY_DIVERSITY       = 0x0A,
    WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_DISABLE_FREQUENCY_DIVERSITY      = 0x0B,

}
wf_gem_hci_command_id_ant_receiver_e;


/// HCI_MSG_CLASS_ANT_RECEIVER
typedef enum
{
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_DISCOVERED_V1                       = 0x01,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DISCOVERY_TIMEOUT                          = 0x02,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_CONNECTED                           = 0x03,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECT_DEVICE_TIMEOUT                     = 0x04,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_DISCONNECTED                        = 0x05,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_DISCOVERED                          = 0x06,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_ACK_MSG_STATUS                             = 0x07,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_DEVICE_SERIAL_NUMBER_UPDATED     = 0x20,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_DEVICE_BATTERY_STATUS_UPDATED    = 0x21,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_FEC_BASIC_RESISTANCE_CHANGE      = 0x30,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_FEC_POWER_CHANGE                 = 0x31,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_FEC_WIND_RESISTANCE_CHANGE       = 0x32,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_FEC_TRACK_RESISTANCE_CHANGE      = 0x33,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_FEC_USER_CONFIGURATION_CHANGE    = 0x34,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_HEART_RATE_MONITOR_DATA          = 0x40,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_BIKE_POWER_DATA                  = 0x41,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_BIKE_POWER_CALIBRATION_RESPONSE  = 0x42,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_BIKE_SPEED_DISTANCE_DATA         = 0x60,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_BIKE_CADENCE_DATA                = 0x61,
    WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_FREQUENCY_DIVERSITY_RX_STATUS              = 0x70,
}
wf_gem_hci_event_id_ant_receiver_e;


typedef enum
{
    WF_GEM_HCI_COMMAND_ID_NFC_READER_ENABLE_RADIO                   = 0x01,
    WF_GEM_HCI_COMMAND_ID_NFC_READER_DISABLE_RADIO                  = 0x02,
    WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SCAN_PERIOD                = 0x03,
    WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SCAN_PERIOD                = 0x04,
    WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SUPPORTED_TAGS_TYPES       = 0x05,
    WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SUPPORTED_TAGS_TYPES       = 0x06,
    WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_NFC_RADIO_TEST_MODE        = 0x07,
    WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_SET_NFC_TAG_CONFIGURATION   = 0x08,
    WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_GET_NFC_TAG_CONFIGURATION   = 0x09,
    WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_COMMUNICATION_CHECK         = 0x0A,
} wf_gem_hci_command_id_nfc_reader_t;

typedef enum
{
    WF_GEM_HCI_EVENT_ID_NFC_READER_READ_EVENT                       = 0x01,
    WF_GEM_HCI_EVENT_ID_NFC_TAG_BOARD_READ_EVENT                    = 0x02,
    WF_GEM_HCI_EVENT_ID_NFC_NDEF_READ_EVENT                         = 0x03,
} wf_gem_hci_event_id_nfc_reader_t;






typedef enum
{
    WF_GEM_HCI_BLUETOOTH_STATE_UNKNOWN			= 0,
    WF_GEM_HCI_BLUETOOTH_STATE_DISCONNECTED,
    WF_GEM_HCI_BLUETOOTH_STATE_ADVERTISING_FAST,
    WF_GEM_HCI_BLUETOOTH_STATE_ADVERTISING_SLOW,
    WF_GEM_HCI_BLUETOOTH_STATE_CONNECTED
}
wf_gem_hci_bluetooth_state_e;

typedef enum
{
    WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_NOT_INCLUDE = 0,
    WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_INCLUDE = 1
} wf_gem_hci_bluetooth_battery_service_e;


typedef enum{
    WF_GEM_HCI_BLUETOOTH_PNP_ID_SIG_COMPANY_ASSIGNED_NUMBER = 1,
    WF_GEM_HCI_BLUETOOTH_PNP_ID_USB_FORUM_ASSIGNED_NUMBER = 2
} wf_gem_hci_bluetooth_vendor_source_id_e;



typedef struct
{
    uint8_t vendor_id;
    uint16_t product_id;
    uint8_t hw_version;
    uint8_t fw_version_major;
    uint8_t fw_version_minor;
    uint8_t fw_version_build;
    uint8_t fw_version_simple;
    uint8_t bl_version_major;
    uint8_t bl_version_vendor;
    uint8_t bl_version_device_variant;
    uint8_t bl_version_revision;
}
wf_gem_hci_system_gem_module_version_info_t;

typedef struct 
{
    uint8_t peer_address[BT_ADDRESS_SIZE];
    uint16_t company_id;
    uint8_t uuid[MAX_STANDARD_LONG_UUID_SIZE];
    uint16_t major;
    uint16_t minor;
    uint8_t name_type;
    uint8_t name_length;
    uint8_t name[32];
    int8_t received_rssi;
    int8_t calibrated_rssi;
} wf_gem_hci_bt_receiver_ibeacon_discovered_event_t;

typedef struct {
    uint8_t hr;
} wf_gem_hci_bt_receiver_heart_rate_data_event_t;

typedef struct {
    uint16_t power;
    uint8_t cadence;
    bool more_data; // Whether data below is valid;
    uint8_t event_count;
    uint16_t accumulated_period;
    uint16_t accumulated_torque;
} wf_gem_hci_ant_receiver_power_data_event_t;


#define WF_GEM_HCI_ANT_RECEIVER_BATTERY_STATUS_NEW      ((uint8_t) 1)
#define WF_GEM_HCI_ANT_RECEIVER_BATTERY_STATUS_GOOD     ((uint8_t) 2)
#define WF_GEM_HCI_ANT_RECEIVER_BATTERY_STATUS_OK       ((uint8_t) 3)
#define WF_GEM_HCI_ANT_RECEIVER_BATTERY_STATUS_LOW      ((uint8_t) 4)
#define WF_GEM_HCI_ANT_RECEIVER_BATTERY_STATUS_CRITICAL ((uint8_t) 5)

typedef struct {
    uint8_t battery_index;
    uint8_t battery_status;
    uint8_t battery_voltage_course;
    uint8_t battery_voltage_fractional;
    uint8_t number_of_batteries;
} wf_gem_hci_ant_receiver_battery_status_data_t;

typedef struct {
    uint16_t manufacturer_id;
    uint16_t model_number;
    uint32_t serial_number;
    uint8_t hw_version;
    uint8_t sw_version_main;
    uint8_t sw_version_supplemental;
} wf_gem_hci_ant_receiver_get_manufacturer_info_t;

#define WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_UUID_SIZE               7
#define WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_ADVERTISING_NAME        16
#define WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_NDEF_PAYLOAD_LENGTH     100

typedef struct
{
    uint8_t tag_board_uuid[WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_UUID_SIZE];
    uint16_t os_supported_flags;
    uint8_t bluetooth_name_length;
    uint8_t unique_bluetooth_advertising_name[WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_ADVERTISING_NAME];
    uint8_t android_ndef_payload_length;
    uint8_t android_ndef_payload_data[WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_NDEF_PAYLOAD_LENGTH];
    uint8_t ios_ndef_payload_length;
    uint8_t ios_ndef_payload_data[WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_NDEF_PAYLOAD_LENGTH];
}
wf_gem_hci_nfc_tag_board_configuration_t;






// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// |																			|
// |		Public Variables													|
// |																			|
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

#ifndef SWIG // Ignore for Java wrapper generation
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// |																			|
// |		Public functions													|
// |																			|
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

// init should only be called once per device powerup.
void wf_gem_hci_manager_init(void);

void wf_gem_hci_manager_start(void);
void wf_gem_hci_manager_stop(void);

void wf_gem_hci_manager_get_library_version(uint8_t *out_vmajor, uint8_t *out_vminor, uint8_t *out_vbuild);


// The application project should forward messages from the wf_gem_hci_comms_on_message_received
// event handler to this method.  
// Note: this method will directly call one of the event handler callbacks if needed -
// care should be taken if this method is being called from an interrupt handler etc.
void wf_gem_hci_manager_process_recevied_message(wf_gem_hci_comms_message_t* message);


// The method should be called when the "retry timer" expires/finishes.  
// See wf_gem_hci_manager_on_begin_retry_timer below for details.
void wf_gem_hci_manager_process_command_retry(void);

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		System Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

/**
 Send the System Command Ping

 @note: Response - wf_gem_hci_manager_on_command_response_system_ping
 */
void wf_gem_hci_manager_send_command_sytem_ping(void);

/**
 Send the System Command Shutdown

 @note: Response - wf_gem_hci_manager_on_command_response_system_shutdown
 */
void wf_gem_hci_manager_send_command_sytem_shutdown(void);

/**
 Send the System Command GEM Version Information

 @note: Response - wf_gem_hci_manager_on_command_response_system_get_gem_module_version_info
 */
void wf_gem_hci_manager_send_command_sytem_get_gem_module_version_information(void);

/**
 Send the System Command for System Reset
 */
void wf_gem_hci_manager_send_command_system_reset(void);

/**
 Send the System Command to initiate the bootloader.
 */
void wf_gem_hci_manager_send_command_sytem_initiate_bootloader(uint8_t bootloader_mode);


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Hardware Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_hardware_set_pin(uint8_t number_of_pins, uint8_t* p_pin_configurations);
void wf_gem_hci_manager_send_command_hardware_get_pin(void);
// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Control Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_bluetooth_control_start_advertising(void);
void wf_gem_hci_manager_send_command_bluetooth_control_stop_advertising(void);
void wf_gem_hci_manager_send_command_bluetooth_control_get_bluetooth_state(void);
void wf_gem_hci_manager_send_command_bluetooth_control_disconnect_central(void);


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Configuration Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_bluetooth_config_get_device_name(void);
void wf_gem_hci_manager_send_command_bluetooth_config_set_device_name(utf8_data_t* device_name);
void wf_gem_hci_manager_send_command_bluetooth_config_get_advertising_rates_and_timeouts(void);
void wf_gem_hci_manager_send_command_bluetooth_config_set_advertising_rates_and_timeouts(uint16_t fast_advertising_interval, uint16_t fast_advertising_timeout, uint16_t normal_advertising_interval, uint16_t normal_advertising_timeout);
// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Device Information Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_bluetooth_info_get_manufacturer_name(void);
void wf_gem_hci_manager_send_command_bluetooth_info_set_manufacturer_name(utf8_data_t* manufacturer_name);
void wf_gem_hci_manager_send_command_bluetooth_info_get_model_number(void);
void wf_gem_hci_manager_send_command_bluetooth_info_set_model_number(utf8_data_t* model_number);
void wf_gem_hci_manager_send_command_bluetooth_info_get_serial_number(void);
void wf_gem_hci_manager_send_command_bluetooth_info_set_serial_number(utf8_data_t* serial_number);
void wf_gem_hci_manager_send_command_bluetooth_info_get_hardware_rev(void);
void wf_gem_hci_manager_send_command_bluetooth_info_set_hardware_rev(utf8_data_t* hardware_revision);
void wf_gem_hci_manager_send_command_bluetooth_info_get_firmware_rev(void);
void wf_gem_hci_manager_send_command_bluetooth_info_set_firmware_rev(utf8_data_t* firmware_revision);
void wf_gem_hci_manager_send_command_bluetooth_info_get_battery_included(void);
void wf_gem_hci_manager_send_command_bluetooth_info_set_battery_included(uint8_t battery_included);
void wf_gem_hci_manager_send_command_bluetooth_info_set_pnp_id(uint8_t vendor_source_id, uint16_t vendor_id, uint16_t product_id, uint16_t product_version);
void wf_gem_hci_manager_send_command_bluetooth_info_get_pnp_id(void);
void wf_gem_hci_manager_send_command_bluetooth_info_get_bscs_service_included(void);
void wf_gem_hci_manager_send_command_bluetooth_info_set_bscs_service_included(uint8_t included);
void wf_gem_hci_manager_send_command_bluetooth_info_get_power_service_included(void);
void wf_gem_hci_manager_send_command_bluetooth_info_set_power_service_included(uint8_t included);

// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Receiver Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_bluetooth_receiver_start_discovery(uint16_t bt_service_id, int8_t minimum_rssi, uint8_t discovery_timeout);
void wf_gem_hci_manager_send_command_bluetooth_receiver_stop_discovery(uint16_t bt_service_id);
void wf_gem_hci_manager_send_command_bluetooth_receiver_start_discovery_ibeacon(uint16_t company_id, const uint8_t* p_uuid, int8_t min_rssi, uint8_t timeout);
void wf_gem_hci_manager_send_command_bluetooth_receiver_connect_device(uint16_t service_id, const uint8_t* p_bluetooth_address, uint8_t connection_timeout, bool use_gymconnect_events);
void wf_gem_hci_manager_send_command_bluetooth_receiver_disconnect_device(uint16_t service_id);

// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Control Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

void wf_gem_hci_manager_send_command_ant_control_get_ant_profile_enable(uint8_t profile);
void wf_gem_hci_manager_send_command_ant_control_set_ant_profile_enable(uint8_t profile, uint8_t response);
void wf_gem_hci_manager_send_command_ant_control_get_ant_profile_frequency_diversity(uint8_t profile);
void wf_gem_hci_manager_send_command_ant_control_set_ant_profile_frequency_diversity(uint8_t profile, uint8_t mode, uint8_t channel_a_index, uint8_t channel_b_index);

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Configuration Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_ant_config_get_ant_device_number(void);
void wf_gem_hci_manager_send_command_ant_config_set_ant_device_number(uint8_t device_number_type, uint32_t device_number);
void wf_gem_hci_manager_send_command_ant_config_get_hardware_version(void);
void wf_gem_hci_manager_send_command_ant_config_set_hardware_version(uint8_t hardware_version);
void wf_gem_hci_manager_send_command_ant_config_get_manufacturer_id(void);
void wf_gem_hci_manager_send_command_ant_config_set_manufacturer_id(uint16_t manufacturer_id);
void wf_gem_hci_manager_send_command_ant_config_get_model_number(void);
void wf_gem_hci_manager_send_command_ant_config_set_model_number(uint16_t model_number);
void wf_gem_hci_manager_send_command_ant_config_get_software_version(void);
void wf_gem_hci_manager_send_command_ant_config_set_software_version(uint8_t main, uint8_t supplemental);
void wf_gem_hci_manager_send_command_ant_config_get_serial_number(void);
void wf_gem_hci_manager_send_command_ant_config_set_serial_number(uint32_t serial_number);

// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Receiver Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_ant_receiver_start_discovery(uint16_t ant_plus_device_profile, uint8_t prox_bin, uint8_t timeout);
void wf_gem_hci_manager_send_command_ant_receiver_stop_discovery(uint16_t ant_plus_device_profile);
void wf_gem_hci_manager_send_command_ant_receiver_connect_device(uint16_t ant_plus_device_profile, uint32_t device_number, uint8_t proximity_bin, uint8_t connection_timeout);
void wf_gem_hci_manager_send_command_ant_receiver_disconnect_device(uint16_t ant_plus_device_profile);
void wf_gem_hci_manager_send_command_ant_receiver_request_calibration(uint16_t ant_plus_device_profile, uint8_t attempts);
void wf_gem_hci_manager_send_command_ant_receiver_get_manufacturer_info(uint16_t ant_plus_device_profile);
void wf_gem_hci_manager_send_command_ant_receiver_request_battery(uint16_t ant_plus_device_profile, uint8_t battery_index);


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		NFC Reader Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_nfc_reader_enable_radio(void);
void wf_gem_hci_manager_send_command_nfc_reader_disable_radio(void);
void wf_gem_hci_manager_send_command_nfc_reader_set_scan_period(uint16_t scan_period_in_milliseconds);
void wf_gem_hci_manager_send_command_nfc_reader_get_scan_period(void);
void wf_gem_hci_manager_send_command_nfc_reader_set_supported_tag_types(uint16_t supported_tags_bitfield);
void wf_gem_hci_manager_send_command_nfc_reader_get_supported_tag_types(void);
void wf_gem_hci_manager_send_command_nfc_reader_set_radio_test_mode(uint8_t mode);
void wf_gem_hci_manager_send_command_nfc_reader_set_tag_board_configuration(const wf_gem_hci_nfc_tag_board_configuration_t* p_config);
void wf_gem_hci_manager_send_command_nfc_reader_get_tag_board_configuration(void);
void wf_gem_hci_manager_send_command_nfc_reader_tag_board_comms_check(void);

// Responses
extern void wf_gem_hci_manager_on_command_response_nfc_reader_enable_radio(uint8_t error_code);
extern void wf_gem_hci_manager_on_response_nfc_reader_get_scan_period(uint16_t scan_period);
extern void wf_gem_hci_manager_on_response_nfc_reader_supported_tag_types(uint16_t supported_tag_types);
extern void wf_gem_hci_manager_on_response_nfc_tag_board_get_configuration(const wf_gem_hci_nfc_tag_board_configuration_t* p_config);

// Events
extern void wf_gem_hci_manager_on_event_nfc_reader_read(uint8_t nfc_tag_type, uint8_t nfc_tag_data_type, uint8_t* p_payload, uint8_t nfc_data_payload_length);
extern void wf_gem_hci_manager_on_event_nfc_tag_board_read(void);
extern void wf_gem_hci_manager_on_event_nfc_ndef_read(uint8_t tnf_type, uint8_t record_type, uint16_t record_payload_length, uint8_t* p_payload);

// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// |																			|
// |		External Event Handlers												|
// |																			|
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

// All of the methods below must be implemented in the project somewhere.


// This callback is used when a message is sent to the GEM module and a retry timer should be started.
// When the retry timer is expire/finished, the application should call the wf_gem_hci_manager_process_command_retry() method.
// The timer must only fire once for each begin call; the timer must not be a repeating timer (it should be "single shot").
// If/when the GEM sends an acknowledgement message, the wf_gem_hci_manager_on_cancel_retry_timer() callback will occur.
extern void wf_gem_hci_manager_on_begin_retry_timer(uint16_t cmd_timeout_ms);

// cancel/stop the timer started in wf_gem_hci_manager_on_begin_retry_timer()
extern void wf_gem_hci_manager_on_cancel_retry_timer(void);


// This callback is called if a command message can not be sent and all retries have failed.
extern void wf_gem_hci_manager_on_command_send_failure(wf_gem_hci_comms_message_t* message);






// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		System Command Responses, Events
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

/**
 Response to the System Ping Message

 @note: In response to command wf_gem_hci_manager_send_command_sytem_ping
 */
extern void wf_gem_hci_manager_on_command_response_system_ping(void);
extern void wf_gem_hci_manager_on_command_response_system_shutdown(uint8_t error_code);
extern void wf_gem_hci_manager_on_command_response_system_get_gem_module_version_info(wf_gem_hci_system_gem_module_version_info_t *version_info);
extern void wf_gem_hci_manager_on_command_response_system_reset(uint8_t error_code);

/**
 Response to the System Shutdown Message

 @note: In response to command wf_gem_hci_manager_send_command_sytem_ping
 */

/**
 Response to the System Shutdown Message

 @note: In response to command wf_gem_hci_manager_send_command_sytem_shutdown
 @param error_code WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS if Success otherwise Error Code
 */
extern void wf_gem_hci_manager_send_command_sytem_shutdown(void);

/**
 Response to System GEM Module Version Informaiton

 @note: In response to command wf_gem_hci_manager_send_command_sytem_get_gem_module_version_information
 @param version_info Version Informaiton
 */
extern void wf_gem_hci_manager_on_command_response_system_get_gem_module_version_info(wf_gem_hci_system_gem_module_version_info_t *version_info);

/**
 Response to System Reset Command

 @note: In response to command wf_gem_hci_manager_send_command_system_reset
 @param error_code WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS if Success otherwise Error Code
 */
extern void wf_gem_hci_manager_on_command_response_system_reset(uint8_t error_code);

/**
 System Power Up Event
 */
extern void wf_gem_hci_manager_on_event_system_powerup(void);

/**
 System Shutdown Event
 */
extern void wf_gem_hci_manager_on_event_system_shutdown(void);

/**
 System Bootloader Intiiated Event
 */
extern void wf_gem_hci_manager_on_event_system_bootloader_initiated(uint8_t interface);

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// 		Hardware Command Responses, Events
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -
extern void wf_gem_hci_manager_on_command_response_hardware_get_pin(uint8_t number_of_pins, uint8_t* p_pin_info);



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// 		Bluetooth Control Command Responses, Events
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
extern void wf_gem_hci_manager_on_command_response_bluetooth_control_start_advertising(uint8_t error_code);
extern void wf_gem_hci_manager_on_command_response_bluetooth_control_stop_advertising(uint8_t error_code);

extern void wf_gem_hci_manager_on_command_response_bluetooth_control_get_bluetooth_state(wf_gem_hci_bluetooth_state_e bluetooth_state);

extern void wf_gem_hci_manager_on_event_bluetooth_control_advertising_timed_out(void);
extern void wf_gem_hci_manager_on_event_bluetooth_control_connected_v1(void);
extern void wf_gem_hci_manager_on_event_bluetooth_control_connected(uint16_t connection_handle, uint8_t peer_address_type, uint8_t* p_peer_address, uint8_t device_type);
extern void wf_gem_hci_manager_on_event_bluetooth_control_disconnected(bool peripheral_solicited, bool central_solicited);


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// Generic Command Responses - i.e error code only.
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
extern void wf_gem_hci_manager_on_command_response_generic(uint8_t error_code);

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// Bluetooth Configuration Command Responses
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
extern void wf_gem_hci_manager_on_command_response_bluetooth_config_get_device_name(utf8_data_t* device_name);
extern void wf_gem_hci_manager_on_command_response_bluetooth_config_set_device_name(uint8_t error_code);
extern void wf_gem_hci_manager_on_command_response_bluetooth_config_get_advertising_intervals_and_timeouts(uint16_t fast_advertising_interval, uint16_t fast_advertising_timeout, uint16_t normal_advertising_interval, uint16_t normal_advertising_timeout);

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Device Information Command Responses
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -
extern void wf_gem_hci_manager_on_command_response_bluetooth_info_get_pnp_id(uint8_t vendor_source_id, uint16_t vendor_id, uint16_t product_id, uint16_t product_version);
extern void wf_gem_hci_manager_on_command_response_bluetooth_info_get_bscs_service_enabled(uint8_t enabled);
extern void wf_gem_hci_manager_on_command_response_bluetooth_info_get_power_service_enabled(uint8_t enabled);



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Device Receiver Command Responses and Events
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
extern void wf_gem_hci_manager_on_command_response_bt_response_start_discovery(uint16_t service_id, uint8_t err_code);
extern void wf_gem_hci_manager_on_command_response_bt_response_stop_discovery(uint16_t service_id, uint8_t err_code);
extern void wf_gem_hci_manager_on_event_bt_receiver_discovery_timeout(uint16_t service_id);
extern void wf_gem_hci_manager_on_command_response_bt_response_connect_device(uint16_t service_id, uint8_t err_code);
extern void wf_gem_hci_manager_on_command_response_bt_response_disconnect_device(uint16_t service_id, uint8_t err_code);
extern void wf_gem_hci_manager_on_event_bt_receiver_device_connected(uint16_t service_id, const uint8_t* bt_address);
extern void wf_gem_hci_manager_on_event_bt_receiver_device_connection_failure(uint16_t service_id, uint8_t failure_reason);
extern void wf_gem_hci_manager_on_event_bt_receiver_device_disconnected(uint16_t service_id, uint8_t disconnect_reason);
extern void wf_gem_hci_manager_on_event_bt_receiver_device_discovered(uint16_t service_id, const uint8_t* bt_address, uint8_t local_name_type, uint8_t local_name_size, const char* local_name, int8_t rssi);
extern void wf_gem_hci_manager_on_event_bt_receiver_ibeacon_discovered(wf_gem_hci_bt_receiver_ibeacon_discovered_event_t* p_ibeacon_evt);
extern void wf_gem_hci_manager_on_event_bt_receiver_heart_rate_data(wf_gem_hci_bt_receiver_heart_rate_data_event_t* p_hr_evt);
// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Control Command Responses
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -
extern void wf_gem_hci_manager_on_command_response_ant_control_get_ant_profile_enable(uint8_t profile, uint8_t enabled_or_disabled);
extern void wf_gem_hci_manager_on_command_response_ant_control_get_ant_profile_frequency_diversity_enable(
    uint8_t response, 
    uint8_t profile,
    uint8_t mode,
    uint8_t channel_a_frequency_index,
    uint8_t channel_b_frequency_index);



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Configuration Command Responses
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
extern void wf_gem_hci_manager_on_command_response_ant_config_get_ant_device_number(uint8_t type, uint32_t device_number);
extern void wf_gem_hci_manager_on_command_response_ant_config_get_hardware_version(uint8_t hardware_version);
extern void wf_gem_hci_manager_on_command_response_ant_config_get_manufacturer_id(uint16_t manufacturer_id);

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Receiver Command Responses and Events
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
extern void wf_gem_hci_manager_on_command_response_ant_receiver_start_discovery(uint16_t profile, uint8_t error_code);
extern void wf_gem_hci_manager_on_command_response_ant_receiver_stop_discovery(uint16_t profile, uint8_t error_code);
extern void wf_gem_hci_manager_on_command_response_ant_receiver_connect_device(uint16_t profile, uint8_t error_code);
extern void wf_gem_hci_manager_on_command_response_ant_receiver_disconnect_device(uint16_t profile, uint8_t error_code);
extern void wf_gem_hci_manager_on_command_response_ant_receiver_request_calibration(uint16_t profile, uint8_t error_code);
extern void wf_gem_hci_manager_on_command_response_ant_receiver_get_manufacturer_info(uint16_t profile, uint8_t error_code, const wf_gem_hci_ant_receiver_get_manufacturer_info_t* p_data);
extern void wf_gem_hci_manager_on_command_response_ant_receiver_request_battery_status(uint16_t profile, uint8_t error_code, const wf_gem_hci_ant_receiver_battery_status_data_t* p_data);

extern void wf_gem_hci_manager_on_event_ant_receiver_discovery_timeout(uint16_t profile_id);
extern void wf_gem_hci_manager_on_event_ant_receiver_device_connected(uint16_t ant_plus_profile_id, uint32_t device_number);
extern void wf_gem_hci_manager_on_event_ant_receiver_connected_device_timeout(uint16_t ant_plus_profile_id);
extern void wf_gem_hci_manager_on_event_ant_receiver_device_disconnected(uint16_t ant_plus_profile_id, uint8_t disconnect_reason);
extern void wf_gem_hci_manager_on_event_ant_receiver_device_discovered(uint16_t ant_plus_profile_id, uint32_t device_number, int8_t rssi);
extern void wf_gem_hci_manager_on_event_ant_receiver_heart_rate_data(uint8_t heart_rate);
extern void wf_gem_hci_manager_on_event_ant_receiver_power_data(const wf_gem_hci_ant_receiver_power_data_event_t* p_power);
extern void wf_gem_hci_manager_on_event_ant_receiver_power_calibration_response(uint8_t calibration_response, uint8_t auto_zero_support, uint8_t auto_zero_enabled, uint16_t calibration_data);
// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Receiver Command Responses
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -

#endif // #ifndef SWIG
#endif 

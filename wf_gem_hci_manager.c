//  Copyright (c) 2012-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#include "wf_gem_hci_manager.h"
#include "wf_gem_hci_config.h"
#include "wf_gem_hci_library_version.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "utils.h"

// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Macros and Definitions										|
// |																								|
// +------------------------------------------------------------------------------------------------+


#define WF_GEM_HCI_DEVICE_NAME_MAX_LEN  32
#define WF_GEM_HCI_MANUFACTURER_NAME_MAX_LEN  32    // TO-DO - check in FW what the max is.
#define WF_GEM_HCI_MODEL_NUMBER_MAX_LEN  32    // TO-DO - check in FW what the max is.
#define WF_GEM_HCI_SERIAL_NUMBER_MAX_LEN  32    // TO-DO - check in FW what the max is.
#define WF_GEM_HCI_HARDWARE_REVISION_MAX_LEN  32    // TO-DO - check in FW what the max is.
#define WF_GEM_HCI_FIRMWARE_REVISION_MAX_LEN  32    // TO-DO - check in FW what the max is.





// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Public Variables											|
// |																								|
// +------------------------------------------------------------------------------------------------+


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Private Variables											|
// |																								|
// +------------------------------------------------------------------------------------------------+

static struct outstanding_command_t
{
    bool                        is_outstanding;
    uint8_t                     max_retries;
    uint8_t 	                retry_attempts;
    uint16_t	                cmd_timeout_ms;
    wf_gem_hci_comms_message_t  command_message;
}
_outstanding_command;



// +------------------------------------------------------------------------------------------------+
// |																								|
// |							Private Function Declarations										|
// |																								|
// +------------------------------------------------------------------------------------------------+

#define SEND_CMD_MSG    _wf_gem_hci_manager_send_command_message
void _wf_gem_hci_manager_send_command_message(uint8_t message_class_id, uint8_t message_id, uint8_t data_length, const uint8_t* const data, uint8_t max_retries, uint16_t cmd_timeout_ms);

static void handle_command_send_failure(wf_gem_hci_comms_message_t* message);

static void process_recevied_message(wf_gem_hci_comms_message_t* message);


// +------------------------------------------------------------------------------------------------+
// |																								|
// |							Protected Function Declarations										|
// |																								|
// +------------------------------------------------------------------------------------------------+

#if WF_GEM_HCI_CONFIG_INCLUDE_GYMCONNECT_MANAGER
void _wf_gem_hci_manager_gymconnect_handle_incoming_message(wf_gem_hci_comms_message_t* message);
void _wf_gem_hci_manager_gymconnect_on_command_send_failure_internal(wf_gem_hci_comms_message_t* message);
#endif


#if WF_GEM_HCI_CONFIG_INCLUDE_BOOTLOADER_MANAGER
void _wf_gem_hci_manager_bootloader_handle_incoming_message(wf_gem_hci_comms_message_t* message);
void _wf_gem_hci_manager_bootloader_on_command_send_failure_internal(wf_gem_hci_comms_message_t* message);
#endif


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Public Functions											|
// |																								|
// +------------------------------------------------------------------------------------------------+

void wf_gem_hci_manager_init(void)
{

}

void wf_gem_hci_manager_start(void)
{

}

void wf_gem_hci_manager_stop(void)
{

}


void wf_gem_hci_manager_get_library_version(uint8_t *out_vmajor, uint8_t *out_vminor, uint8_t *out_vbuild)
{
    *out_vmajor = GEM_HCI_LIBRARY_VERSION_MAJOR;
    *out_vminor = GEM_HCI_LIBRARY_VERSION_MINOR;
    *out_vbuild = GEM_HCI_LIBRARY_VERSION_BUILD;
}


void wf_gem_hci_manager_process_recevied_message(wf_gem_hci_comms_message_t* message)
{
    process_recevied_message(message);
}


void wf_gem_hci_manager_process_command_retry(void)
{
    if (_outstanding_command.is_outstanding)
    {
        if (_outstanding_command.retry_attempts >= _outstanding_command.max_retries)
        {
            // failure.
            _outstanding_command.is_outstanding = false;
            wf_gem_hci_manager_on_cancel_retry_timer();
            handle_command_send_failure(&_outstanding_command.command_message);
        }
        else
        {
            _outstanding_command.retry_attempts++;
            wf_gem_hci_comms_send_message(&_outstanding_command.command_message);
            wf_gem_hci_manager_on_begin_retry_timer(_outstanding_command.cmd_timeout_ms);
        }
    }
}


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		System Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_sytem_ping(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_SYSTEM,
                 WF_GEM_HCI_COMMAND_ID_SYSTEM_PING,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_sytem_shutdown(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_SYSTEM,
                 WF_GEM_HCI_COMMAND_ID_SYSTEM_SHUTDOWN,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_sytem_get_gem_module_version_information(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_SYSTEM,
                 WF_GEM_HCI_COMMAND_ID_SYSTEM_GET_GEM_MODULE_VERSION_INFO,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_system_reset(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_SYSTEM,
                 WF_GEM_HCI_COMMAND_ID_SYSTEM_RESET,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_sytem_initiate_bootloader(uint8_t bootloader_mode)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_SYSTEM,
                WF_GEM_HCI_COMMAND_ID_SYSTEM_INITIATE_BOOTLOADER,
                1,
                &bootloader_mode,
                WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);

}

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Hardware Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

void wf_gem_hci_manager_send_command_hardware_set_pin(uint8_t number_of_pins, uint8_t* p_pin_configurations)
{
    // ASSUMES p_pin_configurations size is correct - 2 bytes per pin.
    uint8_t size_of_message = 2*number_of_pins;

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_HARDWARE,
                WF_GEM_HCI_COMMAND_ID_HARDWARE_SET_PIN_IO_CONFIG,
                size_of_message,
                p_pin_configurations,
                WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                WF_GEM_HCI_LONG_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_hardware_get_pin(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_HARDWARE,
                WF_GEM_HCI_COMMAND_ID_HARDWARE_GET_PIN_IO_CONFIG,
                0,
                NULL,
                WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);

}
// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Control Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_bluetooth_control_start_advertising(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_CONTROL,
                 WF_GEM_HCI_COMMAND_ID_BT_CONTROL_START_ADV,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_control_stop_advertising(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_CONTROL,
                 WF_GEM_HCI_COMMAND_ID_BT_CONTROL_STOP_ADV,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_control_get_bluetooth_state(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_CONTROL,
                 WF_GEM_HCI_COMMAND_ID_BT_CONTROL_GET_STATE,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_control_disconnect_central(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_CONTROL,
                 WF_GEM_HCI_COMMAND_ID_BT_CONTROL_DISCONNECT_CENTRAL,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Configuration Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_bluetooth_config_get_device_name(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_DEVICE_NAME,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_config_set_device_name(utf8_data_t* device_name)
{
    uint8_t name_size_w_term = strlen(device_name) + 1; // +1 to include terminator in size
    if (name_size_w_term > WF_GEM_HCI_DEVICE_NAME_MAX_LEN)
    {
        // too long - don't send now.
        return;
    }
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_DEVICE_NAME,
                 name_size_w_term,
                 (uint8_t*)device_name,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_config_get_advertising_rates_and_timeouts(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_ADV_TIMING,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_config_set_advertising_rates_and_timeouts(
    uint16_t fast_advertising_interval,
    uint16_t fast_advertising_timeout,
    uint16_t normal_advertising_interval,
    uint16_t normal_advertising_timeout)
{
    uint8_t buffer[8];

    buffer[0] = (uint8_t) (fast_advertising_interval >> 0);
    buffer[1] = (uint8_t) (fast_advertising_interval >> 8);
    buffer[2] = (uint8_t) (fast_advertising_timeout >> 0);
    buffer[3] = (uint8_t) (fast_advertising_timeout >> 8);
    buffer[4] = (uint8_t) (normal_advertising_interval >> 0);
    buffer[5] = (uint8_t) (normal_advertising_interval >> 8);
    buffer[6] = (uint8_t) (normal_advertising_interval >> 0);
    buffer[7] = (uint8_t) (normal_advertising_interval >> 8);

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_ADV_TIMING,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Device Information Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_bluetooth_info_get_manufacturer_name(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_MANU_NAME,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_set_manufacturer_name(utf8_data_t* manufacturer_name)
{
    uint8_t name_size_w_term = strlen(manufacturer_name) + 1; // +1 to include terminator in size
    if (name_size_w_term > WF_GEM_HCI_MANUFACTURER_NAME_MAX_LEN)
    {
        // too long - don't send now.
        return;
    }
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MANU_NAME,
                 name_size_w_term,
                 (uint8_t*)manufacturer_name,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_get_model_number(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_MODEL_NUM,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_set_model_number(utf8_data_t* model_number)
{
    uint8_t name_size_w_term = strlen(model_number) + 1; // +1 to include terminator in size
    if (name_size_w_term > WF_GEM_HCI_MODEL_NUMBER_MAX_LEN)
    {
        // too long - don't send now.
        return;
    }
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MODEL_NUM,
                 name_size_w_term,
                 (uint8_t*)model_number,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_get_serial_number(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_SERIAL_NUM,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_set_serial_number(utf8_data_t* serial_number)
{
    uint8_t name_size_w_term = strlen(serial_number) + 1; // +1 to include terminator in size
    if (name_size_w_term > WF_GEM_HCI_SERIAL_NUMBER_MAX_LEN)
    {
        // too long - don't send now.
        return;
    }
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_SERIAL_NUM,
                 name_size_w_term,
                 (uint8_t*)serial_number,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_get_hardware_rev(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_HW_REV,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_set_hardware_rev(utf8_data_t* hardware_revision)
{
    uint8_t name_size_w_term = strlen(hardware_revision) + 1; // +1 to include terminator in size
    if (name_size_w_term > WF_GEM_HCI_HARDWARE_REVISION_MAX_LEN)
    {
        // too long - don't send now.
        return;
    }
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_HW_REV,
                 name_size_w_term,
                 (uint8_t*)hardware_revision,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_get_firmware_rev(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_FW_REV,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_set_firmware_rev(utf8_data_t* firmware_revision)
{
    uint8_t name_size_w_term = strlen(firmware_revision) + 1; // +1 to include terminator in size
    if (name_size_w_term > WF_GEM_HCI_FIRMWARE_REVISION_MAX_LEN)
    {
        // too long - don't send now.
        return;
    }
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_FW_REV,
                 name_size_w_term,
                 (uint8_t*)firmware_revision,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_get_battery_included(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_BATT_SERV_INC,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_set_battery_included(uint8_t battery_included)
{
    if( battery_included != WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_NOT_INCLUDE &&
        battery_included != WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_INCLUDE)
    {
        return;
    }
    uint8_t buffer[1] = {battery_included};

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BATT_SERV_INC,
                 1,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}


void wf_gem_hci_manager_send_command_bluetooth_info_set_pnp_id(
    uint8_t vendor_source_id,
    uint16_t vendor_id,
    uint16_t product_id,
    uint16_t product_version)
{
    if( vendor_source_id != WF_GEM_HCI_BLUETOOTH_PNP_ID_SIG_COMPANY_ASSIGNED_NUMBER &&
        vendor_source_id != WF_GEM_HCI_BLUETOOTH_PNP_ID_USB_FORUM_ASSIGNED_NUMBER)
    {
        return;
    }
    uint8_t buffer[7];
    buffer[0] = vendor_source_id;
    buffer[1] = (uint8_t) (vendor_id >> 0);
    buffer[2] = (uint8_t) (vendor_id >> 8);
    buffer[3] = (uint8_t) (product_id >> 0);
    buffer[4] = (uint8_t) (product_id >> 8);
    buffer[5] = (uint8_t) (product_version >> 0);
    buffer[6] = (uint8_t) (product_version >> 8);

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_PNP_ID,
                 7,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_get_pnp_id(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_PNP_ID,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_set_power_service_included(
    uint8_t included)
{
    if( included != 0 &&
        included != 1)
    {
        return;
    }
    int length = 0;
    uint8_t buffer[1];
    buffer[length++] = included;
    
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_POWER_SERVICE_ENABLED,
                 length,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_get_power_service_included(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_POWER_SERVICE_ENABLED,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_set_bscs_service_included(
    uint8_t included)
{
    if( included != 0 &&
        included != 1)
    {
        return;
    }
    int length = 0;
    uint8_t buffer[1];
    buffer[length++] = included;
    
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BSCS_SERVICE_ENABLED,
                 length,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_info_get_bscs_service_included(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO,
                 WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_BSCS_SERVICE_ENABLED,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Receiver Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

void wf_gem_hci_manager_send_command_bluetooth_receiver_start_discovery(uint16_t bt_service_id, int8_t minimum_rssi, uint8_t discovery_timeout)
{
    uint8_t buffer[4];
    int index = 0;

    buffer[index++] = (uint8_t)(bt_service_id >> 0);
    buffer[index++] = (uint8_t)(bt_service_id >> 8);
    buffer[index++] = (uint8_t)minimum_rssi;
    buffer[index++] = discovery_timeout;

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_START_DISCOVERY,
                 index,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_receiver_stop_discovery(uint16_t bt_service_id)
{
    uint8_t buffer[2];
    int index = 0;

    buffer[index++] = (uint8_t)(bt_service_id >> 0);
    buffer[index++] = (uint8_t)(bt_service_id >> 8);

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_STOP_DISCOVERY,
                 index,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_receiver_start_discovery_ibeacon(uint16_t company_id, const uint8_t* p_uuid, int8_t min_rssi, uint8_t timeout)
{
    uint8_t buffer[20];
    int index = 0;

    buffer[index++] = (uint8_t)(company_id >> 0);
    buffer[index++] = (uint8_t)(company_id >> 8);
    memcpy(&buffer[index], p_uuid, MAX_STANDARD_LONG_UUID_SIZE);
    index += MAX_STANDARD_LONG_UUID_SIZE;

    buffer[index++] = min_rssi;
    buffer[index++] = timeout;

 
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_START_DISCOVERY_IBEACON,
                 index,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_receiver_connect_device(uint16_t service_id, const uint8_t* p_bluetooth_address, uint8_t connection_timeout, bool use_gymconnect_events)
{
    uint8_t buffer[9];
    int index = 0;

    memcpy(&buffer[index], &service_id, sizeof(uint16_t));
    index += sizeof(uint16_t);
    memcpy(&buffer[index], p_bluetooth_address, BT_ADDRESS_SIZE);
    index += BT_ADDRESS_SIZE;
    buffer[index++] = connection_timeout;

    buffer[index++] = use_gymconnect_events == true ? 1 : 0; // Enable Gymconnect Events
    

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_CONNECT_DEVICE,
                 index,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_bluetooth_receiver_disconnect_device(uint16_t service_id)
{
    uint8_t buffer[2];
    int index = 0;

    memcpy(&buffer[index], &service_id, sizeof(uint16_t));
    index += sizeof(uint16_t);
       

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_DISCONNECT_DEVICE,
                 index,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Control Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

void wf_gem_hci_manager_send_command_ant_control_get_ant_profile_enable(uint8_t profile)
{
    uint8_t buffer[] = { profile };

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONTROL,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_ENABLED,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_control_set_ant_profile_enable(uint8_t profile, uint8_t enable_or_disable)
{
    uint8_t buffer[] = { profile, enable_or_disable };

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONTROL,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_ENABLED,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}


void wf_gem_hci_manager_send_command_ant_control_get_ant_profile_frequency_diversity(uint8_t profile)
{
    uint8_t buffer[] = { profile };

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONTROL,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_FREQ_DIV,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_control_set_ant_profile_frequency_diversity(uint8_t profile, uint8_t mode, uint8_t channel_a_index, uint8_t channel_b_index)
{
    uint8_t buffer[] = { profile, mode, channel_a_index, channel_b_index };

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONTROL,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_FREQ_DIV,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Configuration Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+


void wf_gem_hci_manager_send_command_ant_config_get_ant_device_number(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_ANT_DEVICE_NUMBER,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_config_set_ant_device_number(uint8_t device_number_type, uint32_t device_number)
{

    uint8_t buffer[4];
    device_number &= 0x000FFFFF; // Mask of 20 bit just in case.


    buffer[0] = device_number_type; 
    buffer[1] = (uint8_t)((device_number >> 0) & 0xFF);
    buffer[2] = (uint8_t)((device_number >> 8) & 0xFF);
    buffer[3] = (uint8_t)((device_number >> 16) & 0x0F);

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_ANT_DEVICE_NUMBER,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}


void wf_gem_hci_manager_send_command_ant_config_get_hardware_version(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_HW_VER,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_config_set_hardware_version(uint8_t hardware_version)
{
    uint8_t buffer[1] = {hardware_version};

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_HW_VER,
                 1,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}


void wf_gem_hci_manager_send_command_ant_config_get_manufacturer_id(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_MANU_ID,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_config_set_manufacturer_id(uint16_t manufacturer_id)
{
    uint8_t buffer[2];

    buffer[0] = (uint8_t)(manufacturer_id >> 0);
    buffer[1] = (uint8_t)(manufacturer_id >> 8);


    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MANU_ID,
                 2,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_config_get_model_number(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_MODEL_NUM,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_config_set_model_number(uint16_t model_number)
{
    uint8_t buffer[2];

    buffer[0] = (uint8_t)(model_number >> 0);
    buffer[1] = (uint8_t)(model_number >> 8);


    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MODEL_NUM,
                 2,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_config_get_software_version(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_SW_VER,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_config_set_software_version(uint8_t main, uint8_t supplemental)
{
    uint8_t buffer[2];

    buffer[0] = main;
    buffer[1] = supplemental;


    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SW_VER,
                 2,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_config_get_serial_number(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_SERIAL_NUMBER,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_config_set_serial_number(uint32_t serial_number)
{
    uint8_t buffer[4];

    buffer[0] = (uint8_t)(serial_number >> 0);
    buffer[1] = (uint8_t)(serial_number >> 8);
    buffer[2] = (uint8_t)(serial_number >> 16);
    buffer[3] = (uint8_t)(serial_number >> 24);


    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_CONFIG,
                 WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SERIAL_NUMBER,
                 4,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Receiver Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_ant_receiver_start_discovery(uint16_t ant_plus_device_profile, uint8_t prox_bin, uint8_t timeout)
{
    uint8_t buffer[4];

    // TO DO - check args!!

    buffer[0] = (uint8_t)(ant_plus_device_profile >> 0);
    buffer[1] = (uint8_t)(ant_plus_device_profile >> 8);
    buffer[2] = prox_bin;
    buffer[3] = timeout;

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_START_DISCOVERY,
                 4,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_receiver_stop_discovery(uint16_t ant_plus_device_profile)
{
    uint8_t buffer[2];

    buffer[0] = (uint8_t)(ant_plus_device_profile >> 0);
    buffer[1] = (uint8_t)(ant_plus_device_profile >> 8);

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_STOP_DISCOVERY,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}


void wf_gem_hci_manager_send_command_ant_receiver_connect_device(uint16_t ant_plus_device_profile, uint32_t device_number, uint8_t proximity_bin, uint8_t connection_timeout)
{
    uint8_t buffer[8];
    int index = 0;

    memcpy(&buffer[index], &ant_plus_device_profile, sizeof(uint16_t));
    index += sizeof(uint16_t);

    memcpy(&buffer[index], &device_number, sizeof(uint32_t));
    index += sizeof(uint32_t);

    buffer[index++] = proximity_bin;
    buffer[index++] = connection_timeout;

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_CONNECT_DEVICE,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_receiver_disconnect_device(uint16_t ant_plus_device_profile)
{
    uint8_t buffer[2];

    memcpy(&buffer[0], &ant_plus_device_profile, sizeof(uint16_t));

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_DISCONNECT_DEVICE,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}


void wf_gem_hci_manager_send_command_ant_receiver_request_calibration(uint16_t ant_plus_device_profile, uint8_t attempts)
{
    uint8_t buffer[3];

    memcpy(&buffer[0], &ant_plus_device_profile, sizeof(uint16_t));
    buffer[2] = attempts;

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_REQUEST_CALIBRATION,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_receiver_get_manufacturer_info(uint16_t ant_plus_device_profile)
{
    uint8_t buffer[2];

    memcpy(&buffer[0], &ant_plus_device_profile, sizeof(uint16_t));


    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_MANUFACTURER_INFO,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_ant_receiver_request_battery(uint16_t ant_plus_device_profile, uint8_t battery_index)
{
    uint8_t buffer[3];

    memcpy(&buffer[0], &ant_plus_device_profile, sizeof(uint16_t));
    buffer[2] = battery_index;

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                 WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_BATTERY_STATUS,
                 sizeof(buffer),
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}





// - INCOMPLETE HOST REFERENCE IMPLEMENTATION -
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		NFC Reader Commands
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_send_command_nfc_reader_enable_radio(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                 WF_GEM_HCI_COMMAND_ID_NFC_READER_ENABLE_RADIO,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_nfc_reader_disable_radio(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                 WF_GEM_HCI_COMMAND_ID_NFC_READER_DISABLE_RADIO,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_nfc_reader_set_scan_period(uint16_t scan_period_in_milliseconds)
{
    uint8_t buffer[2];

    buffer[0] = (uint8_t)(scan_period_in_milliseconds >> 0);
    buffer[1] = (uint8_t)(scan_period_in_milliseconds >> 8);

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                 WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SCAN_PERIOD,
                 2,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_nfc_reader_get_scan_period(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SCAN_PERIOD,
                0,
                NULL,
                WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_nfc_reader_set_supported_tag_types(uint16_t supported_tags_bitfield)
{
    uint8_t buffer[2];

    buffer[0] = (uint8_t)(supported_tags_bitfield >> 0);
    buffer[1] = (uint8_t)(supported_tags_bitfield >> 8);

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                 WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SUPPORTED_TAGS_TYPES,
                 2,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_nfc_reader_get_supported_tag_types(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SUPPORTED_TAGS_TYPES,
                0,
                NULL,
                WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_nfc_reader_set_radio_test_mode(uint8_t mode)
{
    uint8_t buffer[1];

    buffer[0] = mode;

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                 WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_NFC_RADIO_TEST_MODE,
                 1,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_nfc_reader_set_tag_board_configuration(const wf_gem_hci_nfc_tag_board_configuration_t* p_config)
{
    uint16_t index = 0;
    uint8_t buffer[sizeof(wf_gem_hci_nfc_tag_board_configuration_t)];
    
    if(p_config->bluetooth_name_length > WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_ADVERTISING_NAME || 
       p_config->ios_ndef_payload_length > WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_NDEF_PAYLOAD_LENGTH ||
       p_config->android_ndef_payload_length > WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_NDEF_PAYLOAD_LENGTH )
    {
        return;
    }
    
    buffer[index++] = (uint8_t)(p_config->os_supported_flags >> 0);
    buffer[index++] = (uint8_t)(p_config->os_supported_flags  >> 8);
    buffer[index++] = p_config->bluetooth_name_length;
    memcpy(&buffer[index],  p_config->unique_bluetooth_advertising_name, p_config->bluetooth_name_length);
    index += p_config->bluetooth_name_length;
    buffer[index++] = p_config->android_ndef_payload_length;
    memcpy(&buffer[index],  p_config->android_ndef_payload_data, p_config->android_ndef_payload_length);
    index += p_config->android_ndef_payload_length;
    buffer[index++] = p_config->ios_ndef_payload_length;
    memcpy(&buffer[index],  p_config->ios_ndef_payload_data, p_config->ios_ndef_payload_length);
    index += p_config->ios_ndef_payload_length;

    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                 WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_SET_NFC_TAG_CONFIGURATION,
                 index,
                 buffer,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_nfc_reader_get_tag_board_configuration(void)
{


    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                 WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_GET_NFC_TAG_CONFIGURATION,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_send_command_nfc_reader_tag_board_comms_check(void)
{
    SEND_CMD_MSG(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                 WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_COMMUNICATION_CHECK,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}



// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Private Functions											|
// |																								|
// +------------------------------------------------------------------------------------------------+


// this is not static so that we can link it from GymConnect/other add ons
void _wf_gem_hci_manager_cancel_outstanding_command_message(void)
{
    if (_outstanding_command.is_outstanding)
    {
        _outstanding_command.is_outstanding = false;
        wf_gem_hci_manager_on_cancel_retry_timer();
    }
}


// this is not static so that we can link it from GymConnect/other add ons
void _wf_gem_hci_manager_send_command_message(uint8_t message_class_id, uint8_t message_id, uint8_t data_length, const uint8_t* const data, uint8_t max_retries, uint16_t cmd_timeout_ms)
{
    _wf_gem_hci_manager_cancel_outstanding_command_message();

    _outstanding_command.command_message.message_event_flag = false;
    _outstanding_command.command_message.message_class_id = message_class_id;
    _outstanding_command.command_message.message_id = message_id;
    _outstanding_command.command_message.data_length = data_length;
    memcpy(&_outstanding_command.command_message.data, data, data_length);
    _outstanding_command.is_outstanding = true;
    _outstanding_command.retry_attempts = 0;
    _outstanding_command.max_retries = max_retries;
    _outstanding_command.cmd_timeout_ms = cmd_timeout_ms;
    wf_gem_hci_manager_on_begin_retry_timer(_outstanding_command.cmd_timeout_ms);

    wf_gem_hci_comms_send_message(&_outstanding_command.command_message);
}

static bool got_cmd_response(wf_gem_hci_comms_message_t* message)
{
    if (!_outstanding_command.is_outstanding ||
        _outstanding_command.command_message.message_class_id != message->message_class_id ||
        _outstanding_command.command_message.message_id != message->message_id)
    {
        // we weren't expecting a command response
        // or the command response is for a different command than the outstanding command
        return false;
    }

    _outstanding_command.is_outstanding = false;
    wf_gem_hci_manager_on_cancel_retry_timer();

    // we expected this command response
    return true;
}



static void handle_command_send_failure(wf_gem_hci_comms_message_t* message)
{
    switch (message->message_class_id)
    {
#if defined(WF_GEM_HCI_CONFIG_INCLUDE_BOOTLOADER_MANAGER)
        case WF_GEM_HCI_MSG_CLASS_BOOTLOADER:
        _wf_gem_hci_manager_bootloader_on_command_send_failure_internal(message);
        break;
#endif

#if WF_GEM_HCI_CONFIG_INCLUDE_GYMCONNECT_MANAGER
        case WF_GEM_HCI_MSG_CLASS_GYM_CONNECT:
        _wf_gem_hci_manager_gymconnect_on_command_send_failure_internal(message);
        break;
#endif
        default:
        wf_gem_hci_manager_on_command_send_failure(message);
        break;
    }
}



// +------------------------------------------------------------------------------------------------+
// |																								|
// |					Incoming Command Response and Event Handlers								|
// |																								|
// +------------------------------------------------------------------------------------------------+

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		System Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_system_ping(wf_gem_hci_comms_message_t* message)
{
    wf_gem_hci_manager_on_command_response_system_ping();
}

static void handle_cmd_resp_system_shutdown(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_system_shutdown(message->data[0]);
}

static void handle_cmd_resp_system_get_gem_module_version_information(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length < 12)
    {
        // unexpected data length...
        return;
    }
    uint8_t data_index = 0;
    wf_gem_hci_system_gem_module_version_info_t version_info;
    version_info.vendor_id = message->data[data_index++];
    version_info.product_id = message->data[data_index++];
    version_info.product_id |= (((uint16_t)message->data[data_index++]) << 8);
    version_info.hw_version = message->data[data_index++];
    version_info.fw_version_major = message->data[data_index++];
    version_info.fw_version_minor = message->data[data_index++];
    version_info.fw_version_build = message->data[data_index++];
    version_info.fw_version_simple = message->data[data_index++];
    version_info.bl_version_major = message->data[data_index++];
    version_info.bl_version_vendor = message->data[data_index++];
    version_info.bl_version_device_variant = message->data[data_index++];
    version_info.bl_version_revision = message->data[data_index++];
    wf_gem_hci_manager_on_command_response_system_get_gem_module_version_info(&version_info);
}

static void handle_cmd_resp_system_reset(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_system_reset(message->data[0]);
}


static void handle_event_system_power_up(wf_gem_hci_comms_message_t* message)
{
    wf_gem_hci_manager_on_event_system_powerup();
}

static void handle_event_system_shutdown(wf_gem_hci_comms_message_t* message)
{
    wf_gem_hci_manager_on_event_system_shutdown();
}

static void handle_event_system_bootloader_initiated(wf_gem_hci_comms_message_t* message)
{
    uint8_t interface = 0;
    if (message->data_length > 0)
    {
        // unexpected data length...
        interface = message->data[0];
    }
    wf_gem_hci_manager_on_event_system_bootloader_initiated(interface);
}

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Hardware Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_hardware_get_pin_io_config(wf_gem_hci_comms_message_t* message)
{
    uint8_t number_of_pins = message->data_length/2;
    wf_gem_hci_manager_on_command_response_hardware_get_pin(number_of_pins, message->data);
}

static void handle_cmd_resp_hardware_set_pin_io_config(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Control Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_bt_control_start_adv(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_bluetooth_control_start_advertising(message->data[0]);
}

static void handle_cmd_resp_bt_control_stop_adv(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_bluetooth_control_stop_advertising(message->data[0]);
}

static void handle_cmd_resp_bt_control_get_bluetooth_state(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint8_t state_val = message->data[0];
    wf_gem_hci_bluetooth_state_e state = WF_GEM_HCI_BLUETOOTH_STATE_UNKNOWN;
    if (state_val <= (uint8_t)WF_GEM_HCI_BLUETOOTH_STATE_CONNECTED)
    {
        state = (wf_gem_hci_bluetooth_state_e)state_val;
    }
    wf_gem_hci_manager_on_command_response_bluetooth_control_get_bluetooth_state(state);
}

static void handle_event_bt_control_adv_timeout(wf_gem_hci_comms_message_t* message)
{
    wf_gem_hci_manager_on_event_bluetooth_control_advertising_timed_out();
}

static void handle_event_bt_control_central_connected_v1(wf_gem_hci_comms_message_t* message)
{
    wf_gem_hci_manager_on_event_bluetooth_control_connected_v1();
}

static void handle_event_bt_control_central_connected(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    uint16_t connection_handle = (uint16_t)((uint16_t)message->data[0] | ((uint16_t)message->data[1] << 8));
    uint8_t peer_address_type = message->data[2];
    uint8_t* p_peer_address = &message->data[3];
    uint8_t device_type = message->data[9];

    wf_gem_hci_manager_on_event_bluetooth_control_connected(connection_handle, peer_address_type, p_peer_address, device_type);
}


static void handle_event_bt_control_central_disconnected(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    bool peripheral_solicited = message->data[0] == 1;
    bool central_solicited = message->data[0] == 2;
    wf_gem_hci_manager_on_event_bluetooth_control_disconnected(peripheral_solicited, central_solicited);
}



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Configuration Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_bt_config_get_device_name(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        wf_gem_hci_manager_on_command_response_bluetooth_config_get_device_name("0");
    }
    else
    {
        wf_gem_hci_manager_on_command_response_bluetooth_config_get_device_name((utf8_data_t*)message->data);
    }
}

static void handle_cmd_resp_bt_config_set_device_name(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_bluetooth_config_set_device_name(message->data[0]);
}

static void handle_cmd_resp_bt_config_get_adv_timing(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length < 8)
    {
        // unexpected data length...
        return;
    }
    uint16_t fast_advertising_interval = (uint16_t)((uint16_t)message->data[0] | ((uint16_t)message->data[1] << 8));
    uint16_t fast_advertising_timeout = (uint16_t)((uint16_t)message->data[2] | ((uint16_t)message->data[3] << 8));
    uint16_t normal_advertising_interval = (uint16_t)((uint16_t)message->data[4] | ((uint16_t)message->data[5] << 8));
    uint16_t normal_advertising_timeout = (uint16_t)((uint16_t)message->data[6] | ((uint16_t)message->data[7] << 8));
    wf_gem_hci_manager_on_command_response_bluetooth_config_get_advertising_intervals_and_timeouts(
        fast_advertising_interval, fast_advertising_timeout, 
        normal_advertising_interval, normal_advertising_timeout);
}

static void handle_cmd_resp_bt_config_set_adv_timing(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_config_get_device_addr(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_bt_config_set_device_addr(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_config_get_radio_tx_power(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_bt_config_set_radio_tx_power(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_config_get_connection_interval(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_bt_config_set_connection_interval(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Device Information Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_bt_device_info_get_manu_name(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
    printf("Not Implemented - handle_cmd_resp_bt_device_info_get_manu_name\n");
}

static void handle_cmd_resp_bt_device_info_set_manu_name(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_device_info_get_model_num(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_bt_device_info_set_model_num(wf_gem_hci_comms_message_t* message)
{
        if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_device_info_get_serial_num(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_bt_device_info_set_serial_num(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_device_info_get_hw_rev(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_bt_device_info_set_hw_rev(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_device_info_get_fw_rev(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_bt_device_info_set_fw_rev(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_device_info_get_batt_serv_inc(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_bt_device_info_set_batt_serv_inc(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_device_info_get_reported_batt_lvl(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_bt_device_info_set_reported_batt_lvl(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}


static void handle_cmd_resp_bt_device_info_get_pnp_id(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length < 7)
    {
        // unexpected data length...
        return;
    }
    uint8_t vendor_source_id = message->data[0];
    uint16_t vendor_id = (uint16_t)((uint16_t)message->data[1] | ((uint16_t)message->data[2] << 8));
    uint16_t product_id = (uint16_t)((uint16_t)message->data[3] | ((uint16_t)message->data[4] << 8));
    uint16_t product_version = (uint16_t)((uint16_t)message->data[5] | ((uint16_t)message->data[6] << 8));
  
    wf_gem_hci_manager_on_command_response_bluetooth_info_get_pnp_id(vendor_source_id, vendor_id, product_id, product_version);
}

static void handle_cmd_resp_bt_device_info_set_pnp_id(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_bt_device_info_get_bscs_service_included(wf_gem_hci_comms_message_t* message)
{
   if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_bluetooth_info_get_bscs_service_enabled(message->data[0]);
}
static void handle_cmd_resp_bt_device_info_set_bscs_service_included(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}
static void handle_cmd_resp_bt_device_info_get_power_service_included(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_bluetooth_info_get_power_service_enabled(message->data[0]);
}
static void handle_cmd_resp_bt_device_info_set_power_service_included(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

 

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Bluetooth Receiver Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_bt_receiver_start_discovery(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    // Service ID
    int index = 0;
    uint16_t service_id;

    memcpy(&service_id, &message->data[index], sizeof(uint16_t));
    index += sizeof(uint16_t);
    uint8_t err_code = message->data[index];

    wf_gem_hci_manager_on_command_response_bt_response_start_discovery(service_id, err_code);
}


static void handle_cmd_resp_bt_receiver_stop_discovery(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    // Service ID
    int index = 0;
    uint16_t service_id;

    memcpy(&service_id, &message->data[index], sizeof(uint16_t));
    index += sizeof(uint16_t);
    uint8_t err_code = message->data[index];

    wf_gem_hci_manager_on_command_response_bt_response_stop_discovery(service_id, err_code);
}

static void handle_cmd_resp_bt_receiver_connect_device(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    // Service ID
    int index = 0;
    uint16_t service_id;

    memcpy(&service_id, &message->data[index], sizeof(uint16_t));
    index += sizeof(uint16_t);
    uint8_t err_code = message->data[index];

    wf_gem_hci_manager_on_command_response_bt_response_connect_device(service_id, err_code);
}

static void handle_cmd_resp_bt_receiver_disconnect_device(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    // Service ID
    int index = 0;
    uint16_t service_id;

    memcpy(&service_id, &message->data[index], sizeof(uint16_t));
    index += sizeof(uint16_t);
    uint8_t err_code = message->data[index];

    wf_gem_hci_manager_on_command_response_bt_response_disconnect_device(service_id, err_code);
}

static void handle_cmd_resp_bt_receiver_start_discovery_ibeacon(wf_gem_hci_comms_message_t* message)
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}


static void handle_event_bt_receiver_discovery_timeout(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint16_t service_id;
    memcpy(&service_id, &message->data[0], sizeof(uint16_t));
    wf_gem_hci_manager_on_event_bt_receiver_discovery_timeout(service_id);
}

static void handle_event_bt_receiver_device_connected(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint16_t service_id;
    uint8_t address[6];
    memcpy(&service_id, &message->data[0], sizeof(uint16_t));
    memcpy(address, &message->data[2], sizeof(address));

    wf_gem_hci_manager_on_event_bt_receiver_device_connected(service_id, address);
}
static void handle_event_bt_receiver_device_connection_failure(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint16_t service_id;
    uint8_t failure_reason = message->data[2];
    memcpy(&service_id, &message->data[0], sizeof(uint16_t));
    wf_gem_hci_manager_on_event_bt_receiver_device_connection_failure(service_id, failure_reason);
}

static void handle_event_bt_receiver_device_device_disconnected(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint16_t service_id;
    uint8_t disconnect_reason = message->data[2];
    memcpy(&service_id, &message->data[0], sizeof(uint16_t));
    wf_gem_hci_manager_on_event_bt_receiver_device_disconnected(service_id, disconnect_reason);
}

static void handle_event_bt_receiver_device_device_discovered(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint16_t service_id;
    uint8_t bt_address[BT_ADDRESS_SIZE];
    uint8_t local_name_type;
    uint8_t local_name_size;
    char local_name[BT_MAX_LOCAL_NAME];
    int8_t rssi;

    int index = 0;
    memcpy(&service_id, &message->data[0], sizeof(uint16_t));
    index += sizeof(uint16_t);
    memcpy(bt_address, &message->data[index], BT_ADDRESS_SIZE);
    index += BT_ADDRESS_SIZE;
    local_name_type = message->data[index++];
    local_name_size = message->data_length - BT_ADDRESS_SIZE - sizeof(uint16_t) - 2*sizeof(uint8_t);
    memcpy(local_name, &message->data[index], local_name_size);
    index += local_name_size;
    rssi = (int8_t)message->data[index];

    wf_gem_hci_manager_on_event_bt_receiver_device_discovered(service_id, bt_address, local_name_type, local_name_size, local_name, rssi);

}


static void handle_event_bt_receiver_device_discovered_ibeacon(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_bt_receiver_ibeacon_discovered_event_t ibeacon_info;
    memset(&ibeacon_info, 0, sizeof(wf_gem_hci_bt_receiver_ibeacon_discovered_event_t));

    int index = 0;

    // Peer address
    memcpy(ibeacon_info.peer_address, &message->data[index], BT_ADDRESS_SIZE);
    index += BT_ADDRESS_SIZE;

    // Company ID
    memcpy(&ibeacon_info.company_id, &message->data[index], sizeof(uint16_t));
    index += sizeof(uint16_t);

    // UUID
    memcpy(ibeacon_info.uuid, &message->data[index], MAX_STANDARD_LONG_UUID_SIZE);
    index += MAX_STANDARD_LONG_UUID_SIZE;

    // Major
    memcpy(&ibeacon_info.major, &message->data[index], sizeof(uint16_t));
    index += sizeof(uint16_t);

    // Minor
    memcpy(&ibeacon_info.minor, &message->data[index], sizeof(uint16_t));
    index += sizeof(uint16_t);

    // Name Type
    ibeacon_info.name_type = message->data[index++];

    // Name Length
    ibeacon_info.name_length = message->data[index++];

     // Name
    memcpy(&ibeacon_info.name, &message->data[index], ibeacon_info.name_length);
    index += ibeacon_info.name_length;

    // Received RSSI
    ibeacon_info.received_rssi = (int8_t)message->data[index++];

    // Calibrated RSSI
    ibeacon_info.calibrated_rssi = (int8_t)message->data[index++];

    //int8_t rssi = (int8_t) message->data[6];

    wf_gem_hci_manager_on_event_bt_receiver_ibeacon_discovered(&ibeacon_info);
}




static void handle_event_bt_receiver_heart_rate(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_bt_receiver_heart_rate_data_event_t heart_rate;
    memset(&heart_rate, 0, sizeof(wf_gem_hci_bt_receiver_heart_rate_data_event_t));

    heart_rate.hr = message->data[0];

    wf_gem_hci_manager_on_event_bt_receiver_heart_rate_data(&heart_rate);
}



    // Company ID
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Control Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_ant_control_get_antp_profile_enabled(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint8_t profile = message->data[0];
    uint8_t response = message->data[1];

    wf_gem_hci_manager_on_command_response_ant_control_get_ant_profile_enable(profile, response);
}

static void handle_cmd_resp_ant_control_set_antp_profile_enabled(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_ant_control_get_antp_frequency_diversity_enabled(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint8_t response = message->data[0];
    
    if(response == 0 && message->data_length >= 5) {
        uint8_t profile = message->data[1];
        uint8_t mode = message->data[2];
        uint8_t channel_a_frequency_index = message->data[3];
        uint8_t channel_b_frequency_index = message->data[4];
        wf_gem_hci_manager_on_command_response_ant_control_get_ant_profile_frequency_diversity_enable(
            response, 
            profile,
            mode,
            channel_a_frequency_index,
            channel_b_frequency_index);
    }
    else {
        wf_gem_hci_manager_on_command_response_generic(response);
    }

    
}
static void handle_cmd_resp_ant_control_set_antp_frequency_diversity_enabled(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}
    
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Configuration Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_ant_config_get_ant_device_number(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
    printf("0x%02x, 0x%02x, 0x%02x, 0x%02x", message->data[0], message->data[1], message->data[2], message->data[3]);

    if (message->data_length < 4)
    {
        // Unexpected data length
        return;
    }
    uint8_t type = message->data[0];
    uint32_t device_number = (uint32_t)(((uint32_t)message->data[1] << 0) | 
                                        ((uint32_t)message->data[2] << 8) |
                                        ((uint32_t)message->data[3] << 16));

    

    wf_gem_hci_manager_on_command_response_ant_config_get_ant_device_number(type, device_number);
}

static void handle_cmd_resp_ant_config_set_ant_device_number(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_ant_config_get_radio_tx_power(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_ant_config_set_radio_tx_power(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_ant_config_get_reported_batt_lvl(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_ant_config_set_reported_batt_lvl(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_ant_config_get_hw_ver(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_ant_config_get_hardware_version(message->data[0]);
}

static void handle_cmd_resp_ant_config_set_hw_ver(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_ant_config_get_manu_id(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint16_t manufacturer_id;
    memcpy(&manufacturer_id, message->data, sizeof(uint16_t));
    wf_gem_hci_manager_on_command_response_ant_config_get_manufacturer_id(manufacturer_id);
}

static void handle_cmd_resp_ant_config_set_manu_id(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_ant_config_get_model_num(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_ant_config_set_model_num(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_ant_config_get_sw_ver(wf_gem_hci_comms_message_t* message)
{
    // TODO - not yet implemented
}

static void handle_cmd_resp_ant_config_set_sw_ver(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_ant_config_get_serial_number(wf_gem_hci_comms_message_t* message)
{
    // TO DO - not implemented. 
}

static void handle_cmd_resp_ant_config_set_serial_number(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		ANT+ Receiver Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_ant_receiver_start_discovery(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    uint16_t profile = (uint16_t)((uint16_t)message->data[0] | ((uint16_t)message->data[1] << 8));
    uint8_t error_code = message->data[2];
    wf_gem_hci_manager_on_command_response_ant_receiver_start_discovery(profile, error_code);
}

static void handle_cmd_resp_ant_receiver_stop_discovery(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    uint16_t profile = (uint16_t)((uint16_t)message->data[0] | ((uint16_t)message->data[1] << 8));
    uint8_t error_code = message->data[2];
    wf_gem_hci_manager_on_command_response_ant_receiver_stop_discovery(profile, error_code);
}

static void handle_cmd_resp_ant_receiver_connect_device(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    uint16_t profile = (uint16_t)((uint16_t)message->data[0] | ((uint16_t)message->data[1] << 8));
    uint8_t error_code = message->data[2];
    wf_gem_hci_manager_on_command_response_ant_receiver_connect_device(profile, error_code);
}

static void handle_cmd_resp_ant_receiver_disconnect_device(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    uint16_t profile = (uint16_t)((uint16_t)message->data[0] | ((uint16_t)message->data[1] << 8));
    uint8_t error_code = message->data[2];
    wf_gem_hci_manager_on_command_response_ant_receiver_disconnect_device(profile, error_code);
}


static void handle_cmd_resp_ant_receiver_request_calibration(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint16_t profile = (uint16_t)((uint16_t)message->data[0] | ((uint16_t)message->data[1] << 8));
    uint8_t error_code = message->data[2];

    wf_gem_hci_manager_on_command_response_ant_receiver_request_calibration(profile, error_code);
}


static void handle_cmd_resp_ant_receiver_get_manufacturer_info(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    wf_gem_hci_ant_receiver_get_manufacturer_info_t data;

    uint16_t profile = (uint16_t)((uint16_t)message->data[0] | ((uint16_t)message->data[1] << 8));
    uint8_t error_code = message->data[2];

    if(error_code == WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS)
    {
        int index = 0;
        memcpy(&data.manufacturer_id, &message->data[index], sizeof(uint16_t));
        index += sizeof(uint16_t);

        memcpy(&data.model_number, &message->data[index], sizeof(uint16_t));
        index += sizeof(uint16_t);

        memcpy(&data.serial_number, &message->data[index], sizeof(uint32_t));
        index += sizeof(uint32_t);

        data.hw_version = message->data[index++];
        data.sw_version_main = message->data[index++];
        data.sw_version_supplemental = message->data[index++];
    }

    wf_gem_hci_manager_on_command_response_ant_receiver_get_manufacturer_info(profile, error_code, &data);
}

static void handle_cmd_resp_ant_receiver_get_battery_status(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    wf_gem_hci_ant_receiver_battery_status_data_t data;

    uint16_t profile = (uint16_t)((uint16_t)message->data[0] | ((uint16_t)message->data[1] << 8));
    uint8_t error_code = message->data[2];

    if(error_code == WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS)
    {
        data.battery_status = message->data[3];
        data.battery_voltage_course = message->data[4];
        data.battery_voltage_fractional = message->data[5];
        data.battery_index = message->data[6];
        data.number_of_batteries = message->data[7];
    }

    wf_gem_hci_manager_on_command_response_ant_receiver_request_battery_status(profile, error_code, &data);
}


static void handle_event_ant_receiver_device_discovered(wf_gem_hci_comms_message_t* message)
{
     if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    uint16_t ant_plus_profile_id = (uint16_t)(message->data[0] << 0) | (uint16_t)(message->data[1] << 8);
    uint32_t device_number = (uint32_t)(message->data[2] << 0) | (uint32_t)(message->data[3] << 8) | (uint32_t)(message->data[4] << 16) | (uint32_t)(message->data[5] << 24);
    int8_t rssi = (int8_t) message->data[6];

    wf_gem_hci_manager_on_event_ant_receiver_device_discovered(ant_plus_profile_id, device_number, rssi);
}
static void handle_event_ant_receiver_heart_rate(wf_gem_hci_comms_message_t* message)
{
     if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint8_t heart_rate = message->data[0];

    wf_gem_hci_manager_on_event_ant_receiver_heart_rate_data(heart_rate);
}

static void handle_event_ant_receiver_power(wf_gem_hci_comms_message_t* message)
{
     if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    wf_gem_hci_ant_receiver_power_data_event_t data;
    memset(&data, 0, sizeof(wf_gem_hci_ant_receiver_power_data_event_t));
    data.power = (uint16_t)(message->data[0] << 0) | (uint16_t)(message->data[1] << 8);
    data.cadence = message->data[2];

    if(message->data_length > 3)
    {
        data.more_data = true;
        data.event_count = message->data[3];
        data.accumulated_period = (uint16_t)(message->data[4] << 0) | (uint16_t)(message->data[5] << 8);
        data.accumulated_torque = (uint16_t)(message->data[6] << 0) | (uint16_t)(message->data[7] << 8);
    }

    wf_gem_hci_manager_on_event_ant_receiver_power_data(&data);
}
static void handle_event_ant_receiver_power_calibration_response(wf_gem_hci_comms_message_t* message)
{
     if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }


    uint8_t calibration_response = message->data[0];
    uint8_t auto_zero_support = message->data[1];
    uint8_t auto_zero_enabled = message->data[2];
    uint16_t calibration_data = (uint16_t)(message->data[3] << 0) | (uint16_t)(message->data[4] << 8);;

    wf_gem_hci_manager_on_event_ant_receiver_power_calibration_response(calibration_response, auto_zero_support, auto_zero_enabled, calibration_data);
}




static void handle_event_ant_receiver_device_connected(wf_gem_hci_comms_message_t* message)
{
     if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    uint16_t ant_plus_profile_id = (uint16_t)(message->data[0] << 0) | (uint16_t)(message->data[1] << 8);
    uint32_t device_number = (uint32_t)(message->data[2] << 0) | (uint32_t)(message->data[3] << 8) | (uint32_t)(message->data[4] << 16) | (uint32_t)(message->data[5] << 24);

    wf_gem_hci_manager_on_event_ant_receiver_device_connected(ant_plus_profile_id, device_number);
}

static void handle_event_ant_receiver_connected_device_timeout(wf_gem_hci_comms_message_t* message)
{
     if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    uint16_t ant_plus_profile_id = (uint16_t)(message->data[0] << 0) | (uint16_t)(message->data[1] << 8);
   
    wf_gem_hci_manager_on_event_ant_receiver_connected_device_timeout(ant_plus_profile_id);
}

static void handle_event_ant_receiver_device_disconnected(wf_gem_hci_comms_message_t* message)
{
     if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    uint16_t ant_plus_profile_id = (uint16_t)(message->data[0] << 0) | (uint16_t)(message->data[1] << 8);
    uint8_t disocnnect_reason = message->data[2];
   
    wf_gem_hci_manager_on_event_ant_receiver_device_disconnected(ant_plus_profile_id, disocnnect_reason);
}

static void handle_event_ant_receiver_discovery_timeout(wf_gem_hci_comms_message_t* message)
{
     if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    uint16_t ant_plus_profile_id = (uint16_t)(message->data[0] << 0) | (uint16_t)(message->data[1] << 8);
    

    wf_gem_hci_manager_on_event_ant_receiver_discovery_timeout(ant_plus_profile_id);
}

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		NFC Reader Command Response and Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
static void handle_cmd_resp_nfc_reader_enable_reader(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_nfc_reader_disable_reader(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_nfc_reader_set_scan_period(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_nfc_reader_get_scan_period(wf_gem_hci_comms_message_t* message) 
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint16_t scan_period = (uint16_t)((uint16_t)message->data[0] | (uint16_t)((uint16_t)message->data[1] << 8));
    wf_gem_hci_manager_on_response_nfc_reader_get_scan_period(scan_period);
}

static void handle_cmd_resp_nfc_reader_set_supported_tag_types(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_nfc_reader_get_supported_tag_types(wf_gem_hci_comms_message_t* message) 
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint16_t tag_types = (uint16_t)((uint16_t)message->data[0] | (uint16_t)((uint16_t)message->data[1] << 8));
    wf_gem_hci_manager_on_response_nfc_reader_supported_tag_types(tag_types);
}
    
static void handle_cmd_resp_nfc_reader_set_nfc_radio_test_mode(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_nfc_tag_board_set_configuration(wf_gem_hci_comms_message_t* message) 
{

    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

static void handle_cmd_resp_nfc_tag_board_get_configuration(wf_gem_hci_comms_message_t* message) 
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }

    int index = 0;
    wf_gem_hci_nfc_tag_board_configuration_t config;

    // Tag Board UUID
    memcpy(config.tag_board_uuid, &message->data[index], 7);
    index += 7;
    
    // OS Supported flags
    config.os_supported_flags  = (uint16_t)((uint16_t)message->data[index] | (uint16_t)((uint16_t)message->data[index] << 8));
    index += sizeof(uint16_t);
    
    // Bluetooth Name
    memcpy(config.unique_bluetooth_advertising_name, &message->data[index], 16);
    index += 16;

    // Android Payload length
    config.android_ndef_payload_length  = (uint16_t)((uint16_t)message->data[index] | (uint16_t)((uint16_t)message->data[index] << 8));
    index += sizeof(uint16_t);

    // Android NDEF Payload
    memcpy(config.android_ndef_payload_data, &message->data[index], config.android_ndef_payload_length);
    index += config.android_ndef_payload_length;

    // iOS Payload length
    config.ios_ndef_payload_length  = (uint16_t)((uint16_t)message->data[index] | (uint16_t)((uint16_t)message->data[index] << 8));
    index += sizeof(uint16_t);

    // iOS NDEF Payload
    memcpy(config.ios_ndef_payload_data, &message->data[index], config.ios_ndef_payload_length);
    index += config.ios_ndef_payload_length;

    wf_gem_hci_manager_on_response_nfc_tag_board_get_configuration(&config);
}


static void handle_cmd_resp_nfc_tag_board_communication_check(wf_gem_hci_comms_message_t* message) 
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    
    wf_gem_hci_manager_on_command_response_generic(message->data[0]);
}

    




static void handle_event_nfc_reader_read(wf_gem_hci_comms_message_t* message) 
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    int index = 0;
    uint8_t nfc_tag_type = message->data[index++];
    uint8_t nfc_tag_data_type = message->data[index++];

    uint16_t nfc_data_payload_length = (uint16_t)message->data[index++];
    nfc_data_payload_length |= (uint16_t)((uint16_t)message->data[index++] << 8);


    nfc_data_payload_length = MIN(nfc_data_payload_length, MAX_NFC_READ_PAYLOAD);

    uint8_t payload[MAX_NFC_READ_PAYLOAD];
    memcpy(payload, &message->data[index], nfc_data_payload_length);

    wf_gem_hci_manager_on_event_nfc_reader_read(nfc_tag_type, nfc_tag_data_type, payload, nfc_data_payload_length);
}

static void handle_event_nfc_reader_ndef_read(wf_gem_hci_comms_message_t* message) 
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    int index = 0;
    uint8_t tnf_type = message->data[index++];
    uint8_t record_type = message->data[index++];

    uint16_t record_payload_length = (uint16_t)message->data[index++];
    record_payload_length |= (uint16_t)((uint16_t)message->data[index++] << 8);


    record_payload_length = MIN(record_payload_length, MAX_NFC_READ_PAYLOAD);

    uint8_t payload[MAX_NFC_READ_PAYLOAD];
    memcpy(payload, &message->data[index], record_payload_length);

    wf_gem_hci_manager_on_event_nfc_ndef_read(tnf_type, record_type, record_payload_length, payload);
}

static void handle_event_nfc_tag_board_read(wf_gem_hci_comms_message_t* message) 
{
    if (message->data_length != 0)
    {
        // unexpected data length...
        return;
    }

    wf_gem_hci_manager_on_event_nfc_tag_board_read();
}

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Command Response and Event Handler Lookups
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

typedef void (*function_pointer_cmd_handler_t)(wf_gem_hci_comms_message_t* message);

static const function_pointer_cmd_handler_t SYSTEM_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_system_ping,
    handle_cmd_resp_system_shutdown,
    handle_cmd_resp_system_get_gem_module_version_information,
    handle_cmd_resp_system_reset,
};

static const function_pointer_cmd_handler_t SYSTEM_EVT_HANDLERS[] = {
    NULL,
    handle_event_system_power_up,
    handle_event_system_shutdown,
    handle_event_system_bootloader_initiated
};



static const function_pointer_cmd_handler_t HARDWARE_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_hardware_get_pin_io_config,
    handle_cmd_resp_hardware_set_pin_io_config,
};



static const function_pointer_cmd_handler_t BT_CONTROL_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_bt_control_start_adv,
    handle_cmd_resp_bt_control_stop_adv,
    handle_cmd_resp_bt_control_get_bluetooth_state,
};

static const function_pointer_cmd_handler_t BT_CONTROL_EVT_HANDLERS[] = {
    NULL,
    handle_event_bt_control_adv_timeout,
    handle_event_bt_control_central_connected_v1,
    handle_event_bt_control_central_disconnected,
    handle_event_bt_control_central_connected
};

static const function_pointer_cmd_handler_t ANT_RECEIVER_EVT_HANDLERS[] = {
    NULL,
    NULL,
    handle_event_ant_receiver_discovery_timeout,
    handle_event_ant_receiver_device_connected,
    handle_event_ant_receiver_connected_device_timeout,
    handle_event_ant_receiver_device_disconnected,
    handle_event_ant_receiver_device_discovered
};

static const function_pointer_cmd_handler_t ANT_RECEIVER_EVT_HANDLERS_X40[] = {
    handle_event_ant_receiver_heart_rate,
    handle_event_ant_receiver_power,
    handle_event_ant_receiver_power_calibration_response
};


static const function_pointer_cmd_handler_t BT_CONFIG_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_bt_config_get_device_name,
    handle_cmd_resp_bt_config_set_device_name,
    handle_cmd_resp_bt_config_get_adv_timing,
    handle_cmd_resp_bt_config_set_adv_timing,
    handle_cmd_resp_bt_config_get_device_addr,
    handle_cmd_resp_bt_config_set_device_addr,
    handle_cmd_resp_bt_config_get_radio_tx_power,
    handle_cmd_resp_bt_config_set_radio_tx_power,
    handle_cmd_resp_bt_config_get_connection_interval,
    handle_cmd_resp_bt_config_set_connection_interval,
};



static const function_pointer_cmd_handler_t BT_DEVICE_INFO_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_bt_device_info_get_manu_name,
    handle_cmd_resp_bt_device_info_set_manu_name,
    handle_cmd_resp_bt_device_info_get_model_num,
    handle_cmd_resp_bt_device_info_set_model_num,
    handle_cmd_resp_bt_device_info_get_serial_num,
    handle_cmd_resp_bt_device_info_set_serial_num,
    handle_cmd_resp_bt_device_info_get_hw_rev,
    handle_cmd_resp_bt_device_info_set_hw_rev,
    handle_cmd_resp_bt_device_info_get_fw_rev,
    handle_cmd_resp_bt_device_info_set_fw_rev,
    handle_cmd_resp_bt_device_info_get_batt_serv_inc,
    handle_cmd_resp_bt_device_info_set_batt_serv_inc,
    handle_cmd_resp_bt_device_info_get_reported_batt_lvl,
    handle_cmd_resp_bt_device_info_set_reported_batt_lvl,
    handle_cmd_resp_bt_device_info_get_pnp_id,
    handle_cmd_resp_bt_device_info_set_pnp_id,
    handle_cmd_resp_bt_device_info_get_bscs_service_included,
    handle_cmd_resp_bt_device_info_set_bscs_service_included,
    handle_cmd_resp_bt_device_info_get_power_service_included,
    handle_cmd_resp_bt_device_info_set_power_service_included,
};


static const function_pointer_cmd_handler_t BT_RECEIVER_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_bt_receiver_start_discovery,
    handle_cmd_resp_bt_receiver_stop_discovery,
    handle_cmd_resp_bt_receiver_connect_device,
    handle_cmd_resp_bt_receiver_disconnect_device,
    handle_cmd_resp_bt_receiver_start_discovery_ibeacon
};
//static void handle_event_bt_receiver_heart_rate(wf_gem_hci_comms_message_t* message)
static const function_pointer_cmd_handler_t BT_RECEIVER_EVT_HANDLERS[] = {
    NULL,
    NULL,
    handle_event_bt_receiver_discovery_timeout,
    handle_event_bt_receiver_device_connected,
    handle_event_bt_receiver_device_connection_failure,
    handle_event_bt_receiver_device_device_disconnected,
    handle_event_bt_receiver_device_device_discovered,
    handle_event_bt_receiver_device_discovered_ibeacon,
};

static const function_pointer_cmd_handler_t BT_RECEIVER_EVT_HANDLERS_X40[] = {
    handle_event_bt_receiver_heart_rate,
};

static const function_pointer_cmd_handler_t ANT_CONTROL_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_ant_control_get_antp_profile_enabled,
    handle_cmd_resp_ant_control_set_antp_profile_enabled,
    handle_cmd_resp_ant_control_get_antp_frequency_diversity_enabled,
    handle_cmd_resp_ant_control_set_antp_frequency_diversity_enabled,
};



static const function_pointer_cmd_handler_t ANT_CONFIG_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_ant_config_get_ant_device_number,
    handle_cmd_resp_ant_config_set_ant_device_number,
    handle_cmd_resp_ant_config_get_radio_tx_power,
    handle_cmd_resp_ant_config_set_radio_tx_power,
    handle_cmd_resp_ant_config_get_reported_batt_lvl,
    handle_cmd_resp_ant_config_set_reported_batt_lvl,
    handle_cmd_resp_ant_config_get_hw_ver,
    handle_cmd_resp_ant_config_set_hw_ver,
    handle_cmd_resp_ant_config_get_manu_id,
    handle_cmd_resp_ant_config_set_manu_id,
    handle_cmd_resp_ant_config_get_model_num,
    handle_cmd_resp_ant_config_set_model_num,
    handle_cmd_resp_ant_config_get_sw_ver,
    handle_cmd_resp_ant_config_set_sw_ver,
    handle_cmd_resp_ant_config_get_serial_number,
    handle_cmd_resp_ant_config_set_serial_number,
};

static const function_pointer_cmd_handler_t ANT_RECEIVER_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_ant_receiver_start_discovery,
    handle_cmd_resp_ant_receiver_stop_discovery,
    handle_cmd_resp_ant_receiver_connect_device,
    handle_cmd_resp_ant_receiver_disconnect_device,
    handle_cmd_resp_ant_receiver_request_calibration,
    handle_cmd_resp_ant_receiver_get_manufacturer_info,
    handle_cmd_resp_ant_receiver_get_battery_status
};




static const function_pointer_cmd_handler_t NFC_READER_CMD_HANDLERS[] = {
    NULL,
    handle_cmd_resp_nfc_reader_enable_reader,
    handle_cmd_resp_nfc_reader_disable_reader,
    handle_cmd_resp_nfc_reader_set_scan_period,
    handle_cmd_resp_nfc_reader_get_scan_period,
    handle_cmd_resp_nfc_reader_set_supported_tag_types,
    handle_cmd_resp_nfc_reader_get_supported_tag_types,
    handle_cmd_resp_nfc_reader_set_nfc_radio_test_mode,
    handle_cmd_resp_nfc_tag_board_set_configuration,
    handle_cmd_resp_nfc_tag_board_get_configuration,
    handle_cmd_resp_nfc_tag_board_communication_check
};

static const function_pointer_cmd_handler_t NFC_READER_EVT_HANDLERS[] = {
    NULL,
    handle_event_nfc_reader_read,
    handle_event_nfc_tag_board_read,
    handle_event_nfc_reader_ndef_read,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    handle_event_nfc_tag_board_read,
};


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Event Handlers												|
// |																								|
// +------------------------------------------------------------------------------------------------+

#define ARRAY_LENGTH(ARRAY_NAME)    (sizeof(ARRAY_NAME) / sizeof(*ARRAY_NAME))

#define CALL_CMD_HANDLER(MSG_ID, CMD_HANDLERS_ARRAY_NAME)			\
	do {	                                                        \
		if (MSG_ID < ARRAY_LENGTH(CMD_HANDLERS_ARRAY_NAME))			\
		{															\
			if (CMD_HANDLERS_ARRAY_NAME[MSG_ID]) 					\
			{														\
				CMD_HANDLERS_ARRAY_NAME[MSG_ID](message);			\
				return;												\
			}														\
		}															\
	} while (0)

#define CALL_CMD_HANDLER_WITH_INDEX_OFFSET(MSG_ID, CMD_HANDLERS_ARRAY_NAME, INDEX_OFFSET)							\
	do {																											\
		if (MSG_ID >= INDEX_OFFSET && ((MSG_ID) - (INDEX_OFFSET)) < ARRAY_LENGTH(CMD_HANDLERS_ARRAY_NAME))			\
		{																											\
			if (CMD_HANDLERS_ARRAY_NAME[((MSG_ID) - (INDEX_OFFSET))]) 												\
			{																										\
				CMD_HANDLERS_ARRAY_NAME[((MSG_ID) - (INDEX_OFFSET))](message);										\
				return;																								\
			}																										\
		}																											\
	} while (0)

#define MSG_ID_X40(ID)  (ID & 0x3F)


static void process_recevied_message(wf_gem_hci_comms_message_t* message)
{	
    if (message->message_event_flag)
    {
        // handle events
        switch (message->message_class_id)
        {
            case WF_GEM_HCI_MSG_CLASS_SYSTEM:
            CALL_CMD_HANDLER(message->message_id, SYSTEM_EVT_HANDLERS);
            break;

            case WF_GEM_HCI_MSG_CLASS_BT_CONTROL:
            CALL_CMD_HANDLER(message->message_id, BT_CONTROL_EVT_HANDLERS);
            break;

            case WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER:
            if(message->message_id & 0x40)
                CALL_CMD_HANDLER(MSG_ID_X40(message->message_id), ANT_RECEIVER_EVT_HANDLERS_X40);
            else
                CALL_CMD_HANDLER(message->message_id, ANT_RECEIVER_EVT_HANDLERS);
            break;

            case WF_GEM_HCI_MSG_CLASS_BT_RECEIVER:
            if(message->message_id & 0x40)
                CALL_CMD_HANDLER(MSG_ID_X40(message->message_id), BT_RECEIVER_EVT_HANDLERS_X40);
            else
                CALL_CMD_HANDLER(message->message_id, BT_RECEIVER_EVT_HANDLERS);
            break;

            case WF_GEM_HCI_MSG_CLASS_NFC_READER:
            CALL_CMD_HANDLER(message->message_id, NFC_READER_EVT_HANDLERS);
            break;
            

#if WF_GEM_HCI_CONFIG_INCLUDE_BOOTLOADER_MANAGER
            case WF_GEM_HCI_MSG_CLASS_BOOTLOADER:
            _wf_gem_hci_manager_bootloader_handle_incoming_message(message);
            break;
#endif

#if WF_GEM_HCI_CONFIG_INCLUDE_GYMCONNECT_MANAGER
            case WF_GEM_HCI_MSG_CLASS_GYM_CONNECT:
            _wf_gem_hci_manager_gymconnect_handle_incoming_message(message);
            break;
#endif

            default:
            // ignore the message
            break;
        }
        return;
    }

 
    // handle command responses
    if (!got_cmd_response(message))
    {
        // we did not expect this command response at this time -
        // discard it.
        return;
    }

    switch (message->message_class_id)
    {
        case WF_GEM_HCI_MSG_CLASS_SYSTEM:
        CALL_CMD_HANDLER(message->message_id, SYSTEM_CMD_HANDLERS);
        break;

        case WF_GEM_HCI_MSG_CLASS_HARDWARE:
        CALL_CMD_HANDLER(message->message_id, HARDWARE_CMD_HANDLERS);
        break;

        case WF_GEM_HCI_MSG_CLASS_BT_CONTROL:
        CALL_CMD_HANDLER(message->message_id, BT_CONTROL_CMD_HANDLERS);
        break;

        case WF_GEM_HCI_MSG_CLASS_BT_CONFIG:
        CALL_CMD_HANDLER(message->message_id, BT_CONFIG_CMD_HANDLERS);
        break;

        case WF_GEM_HCI_MSG_CLASS_BT_RECEIVER:
        CALL_CMD_HANDLER(message->message_id, BT_RECEIVER_CMD_HANDLERS);
        break;

        case WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO:
        CALL_CMD_HANDLER(message->message_id, BT_DEVICE_INFO_CMD_HANDLERS);
        break;

        case WF_GEM_HCI_MSG_CLASS_ANT_CONTROL:
        CALL_CMD_HANDLER(message->message_id, ANT_CONTROL_CMD_HANDLERS);
        break;

        case WF_GEM_HCI_MSG_CLASS_ANT_CONFIG:
        CALL_CMD_HANDLER(message->message_id, ANT_CONFIG_CMD_HANDLERS);
        break;

        case WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER:
        CALL_CMD_HANDLER(message->message_id, ANT_RECEIVER_CMD_HANDLERS);

        case WF_GEM_HCI_MSG_CLASS_NFC_READER:
        CALL_CMD_HANDLER(message->message_id, NFC_READER_CMD_HANDLERS);



#if WF_GEM_HCI_CONFIG_INCLUDE_BOOTLOADER_MANAGER
        case WF_GEM_HCI_MSG_CLASS_BOOTLOADER:
        _wf_gem_hci_manager_bootloader_handle_incoming_message(message);
        break;
#endif

#if WF_GEM_HCI_CONFIG_INCLUDE_GYMCONNECT_MANAGER
        case WF_GEM_HCI_MSG_CLASS_GYM_CONNECT:
        _wf_gem_hci_manager_gymconnect_handle_incoming_message(message);
        break;
#endif

        default:
        // ignore the message
        break;
    }

}

//  wf_gem_hci_manager_gymconnect.c
//
//  Created by Chris Uroda on 2016-02-25
//  Copyright (c) 2016 Wahoo Fitness. All rights reserved.
//
//  Copyright (c) 2012-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#include "wf_gem_hci_config.h"
#if WF_GEM_HCI_CONFIG_INCLUDE_GYMCONNECT_MANAGER


#include "wf_gem_hci_manager_gymconnect.h"
#include "../npe_log.h"

#include <stdlib.h>
#include <string.h>

// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Macros and Definitions										|
// |																								|
// +------------------------------------------------------------------------------------------------+


#define ARRAY_LENGTH(ARRAY_NAME)    (sizeof(ARRAY_NAME) / sizeof(*ARRAY_NAME))


#define WF_GEM_HCI_MSG_CLASS_GYM_CONNECT                            0x60

#define WF_GEM_HCI_GYM_CONNECT_FITNESS_EQUIP_PROGRAM_NAME_MAX_LEN   40



// +--------------------------------------------------------------------------+
// |                                                                          |
// |                          HCI_MSG_CLASS_GYM_CONNECT                       |
// |                                                                          |
// +--------------------------------------------------------------------------+






// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Public Variables											|
// |																								|
// +------------------------------------------------------------------------------------------------+


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Private Variables											|
// |																								|
// +------------------------------------------------------------------------------------------------+


static uint32_t	_workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MAX_DATA_FIELDS] = { 0 };
static bool     _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MAX_DATA_FIELDS] = { 0 };






// +------------------------------------------------------------------------------------------------+
// |																								|
// |							Private Function Declarations										|
// |																								|
// +------------------------------------------------------------------------------------------------+

#define send_cmd_msg    _wf_gem_hci_manager_send_command_message
void _wf_gem_hci_manager_send_command_message(uint8_t message_class_id, uint8_t message_id, uint8_t data_length, const uint8_t* const data, uint8_t max_retries, uint16_t cmd_timeout_ms);
void _wf_gem_hci_manager_cancel_outstanding_command_message(void);

static void process_workout_data_update(void);

static void handle_incoming_message(wf_gem_hci_comms_message_t* message);

// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Public Functions											|
// |																								|
// +------------------------------------------------------------------------------------------------+



void wf_gem_hci_manager_gymconnect_get_fe_type(void)
{
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT,
                 WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_TYPE,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS
                 , WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_gymconnect_set_fe_type(wf_gem_hci_gymconnect_fitness_equipment_type_e fe_type)
{
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT,
                 WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TYPE,
                 1,
                 (uint8_t*)&fe_type,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_gymconnect_get_fe_state(void)
{
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT,
                 WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_STATE,
                 0,
                 NULL,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_gymconnect_set_fe_state(wf_gem_hci_gymconnect_fitness_equipment_state_e fe_state)
{
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT,
                 WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_STATE,
                 1, (uint8_t*)&fe_state,
                 WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS,
                 WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_gymconnect_get_fe_workout_program_name(void)
{
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT, WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_WORKOUT_PROG_NAME, 0, NULL, WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS, WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_gymconnect_set_fe_workout_program_name(utf8_data_t* workout_program_name)
{
    uint8_t name_size_w_term = strlen(workout_program_name) + 1; // +1 to include terminator in size
    if (name_size_w_term > WF_GEM_HCI_GYM_CONNECT_FITNESS_EQUIP_PROGRAM_NAME_MAX_LEN)
    {
        // too long - don't send now.
        return;
    }
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT, WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_WORKOUT_PROG_NAME, name_size_w_term, (uint8_t*)workout_program_name, WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS, WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_gymconnect_get_supported_equipment_control_features(void)
{
    send_cmd_msg(
        WF_GEM_HCI_MSG_CLASS_GYM_CONNECT, 
        WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_CONTROL_FEATURES, 
        0, 
        NULL, 
        WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS, 
        WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}

void wf_gem_hci_manager_gymconnect_set_supported_equipment_control_features(uint32_t equipment_control_field_identifier)
{
    uint8_t buffer[sizeof(uint32_t)];

    buffer[0]  = (uint8_t)(equipment_control_field_identifier >> 0);
    buffer[1]  = (uint8_t)(equipment_control_field_identifier >> 8);
    buffer[2]  = (uint8_t)(equipment_control_field_identifier >> 16);
    buffer[3]  = (uint8_t)(equipment_control_field_identifier >> 24);

    send_cmd_msg(
        WF_GEM_HCI_MSG_CLASS_GYM_CONNECT, 
        WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_CONTROL_FEATURES, 
        sizeof(uint32_t),
        buffer, 
        WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS, 
        WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
}



void wf_gem_hci_manager_gymconnect_perform_workout_data_update(void)
{
    process_workout_data_update();
}



void wf_gem_hci_manager_gymconnect_set_workout_data_elapsed_workout_time(uint16_t seconds)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ELAPSED_WORKOUT_TIME] = seconds;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ELAPSED_WORKOUT_TIME] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_elapsed_workout_time(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ELAPSED_WORKOUT_TIME] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_remaining_workout_time(uint16_t seconds)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_REMAINING_WORKOUT_TIME] = seconds;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_REMAINING_WORKOUT_TIME] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_remaining_workout_time(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_REMAINING_WORKOUT_TIME] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_heartrate(uint8_t beats_per_minute)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_HEARTRATE] = beats_per_minute;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_HEARTRATE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_heartrate(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_HEARTRATE] = false;
}

void wf_gem_hci_manager_gymconnect_set_workout_data_avg_heartrate(uint8_t beats_per_minute)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_HR] = beats_per_minute;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_HR] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_avg_heartrate(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_HR] = false;
}

void wf_gem_hci_manager_gymconnect_set_workout_data_max_heartrate(uint8_t beats_per_minute)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MAXIMUM_HR] = beats_per_minute;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MAXIMUM_HR] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_max_heartrate(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MAXIMUM_HR] = false;
}

void wf_gem_hci_manager_gymconnect_set_workout_data_level(uint8_t level)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_LEVEL] = level;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_LEVEL] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_level(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_LEVEL] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_resistance(uint16_t deci_resistance)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_RESISTANCE] = deci_resistance;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_RESISTANCE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_resistance(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_RESISTANCE] = false;
}

void wf_gem_hci_manager_gymconnect_set_workout_data_speed(uint16_t centi_kph)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_SPEED] = centi_kph;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_SPEED] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_speed(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_SPEED] = false;
}

void wf_gem_hci_manager_gymconnect_set_workout_data_current_pace(uint16_t pace_s_per_km)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CURRENT_PACE] = pace_s_per_km;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CURRENT_PACE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_current_pace(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CURRENT_PACE] = false;
}

void wf_gem_hci_manager_gymconnect_set_workout_data_average_pace(uint32_t pace_ms_per_km)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_PACE] = pace_ms_per_km;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_PACE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_average_pace(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_PACE] = false;
}

// Units are "x per minute" where: Bike x = revolutions,  Stair Climber x = steps, Stairmaster x = strides, Cross Trainer x = strides, Rower x = strokes
void wf_gem_hci_manager_gymconnect_set_workout_data_cadence(uint16_t deci_cadence_x_per_minute)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CADENCE] = deci_cadence_x_per_minute;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CADENCE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_cadence(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CADENCE] = false;
}


// Cumulative count of steps/strokes etc.
void wf_gem_hci_manager_gymconnect_set_workout_data_movements_count(uint16_t movements_count)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MOVEMENTS_COUNT] = movements_count;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MOVEMENTS_COUNT] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_movements_count(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MOVEMENTS_COUNT] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_horizontal_distance(uint32_t meters)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_HORIZONTAL_DISTANCE] = meters;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_HORIZONTAL_DISTANCE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_horizontal_distance(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_HORIZONTAL_DISTANCE] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_vertical_distance(uint16_t deci_meters)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_VERTICAL_DISTANCE] = deci_meters;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_VERTICAL_DISTANCE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_vertical_distance(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_VERTICAL_DISTANCE] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_vertical_distance_negative(uint16_t deci_meters)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_VERTICAL_DISTANCE_NEGATIVE] = deci_meters;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_VERTICAL_DISTANCE_NEGATIVE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_vertical_distance_negative(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_VERTICAL_DISTANCE_NEGATIVE] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_energy(uint16_t kilogram_calories)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_ENERGY] = kilogram_calories;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_ENERGY] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_energy(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_ENERGY] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_energy_rate(uint16_t kilogram_calories_per_hour)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ENERGY_RATE] = kilogram_calories_per_hour;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ENERGY_RATE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_energy_rate(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ENERGY_RATE] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_mets(uint8_t deci_mets)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_METS] = deci_mets;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_METS] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_mets(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_METS] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_power(int16_t watts)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_POWER] = watts;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_POWER] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_power(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_POWER] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_torque(uint16_t deci_newton_meters)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_TORQUE] = deci_newton_meters;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_TORQUE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_torque(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_TORQUE] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_gear(uint8_t gear_number)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_GEAR] = gear_number;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_GEAR] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_gear(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_GEAR] = false;
}


// 0% is flat, 100% is 45 degrees uphill, -100% is 45 degrees downhill
void wf_gem_hci_manager_gymconnect_set_workout_data_grade(int16_t deci_grade)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_GRADE] = deci_grade;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_GRADE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_grade(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_GRADE] = false;
}


// 0 degrees is flat, 90 degrees is straight up, -90 degrees is straight down
void wf_gem_hci_manager_gymconnect_set_workout_data_angle(int16_t centi_degrees)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ANGLE] = centi_degrees;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ANGLE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_angle(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ANGLE] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_floor_rate(uint16_t centi_floors_per_minute)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_FLOOR_RATE] = centi_floors_per_minute;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_FLOOR_RATE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_floor_rate(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_FLOOR_RATE] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_floors(uint16_t centi_floors)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_FLOORS] = centi_floors;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_FLOORS] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_floors(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_FLOORS] = false;
}

void wf_gem_hci_manager_gymconnect_set_workout_data_average_power(int16_t power)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_POWER] = (uint16_t)power;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_POWER] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_average_power()
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_POWER] = false;
}

void wf_gem_hci_manager_gymconnect_set_workout_data_average_cadence(uint16_t average_cadence)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_CADENCE] = (uint32_t)average_cadence;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_CADENCE] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_average_cadence()
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_CADENCE] = false;
}


void wf_gem_hci_manager_gymconnect_set_workout_data_average_speed(uint32_t average_speed)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_SPEED] = average_speed;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_SPEED] = true;
}


void wf_gem_hci_manager_gymconnect_clr_workout_data_average_speed(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_SPEED] = false;
}

void wf_gem_hci_manager_gymconnect_set_workout_data_enhanced_speed(uint32_t enhanced_speed)
{
    _workout_data_field_val[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ENHANCED_INST_SPEED] = enhanced_speed;
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ENHANCED_INST_SPEED] = true;
}

void wf_gem_hci_manager_gymconnect_clr_workout_data_enhanced_speed(void)
{
    _workout_data_field_set[WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ENHANCED_INST_SPEED] = false;
}

// +------------------------------------------------------------------------------------------------+
// |																								|
// |								Protected Functions												|
// |																								|
// +------------------------------------------------------------------------------------------------+

void _wf_gem_hci_manager_gymconnect_handle_incoming_message(wf_gem_hci_comms_message_t* message)
{
    handle_incoming_message(message);
}

void _wf_gem_hci_manager_gymconnect_on_command_send_failure_internal(wf_gem_hci_comms_message_t* message)
{
    wf_gem_hci_manager_gymconnect_on_command_send_failure(message);
}


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Private Functions											|
// |																								|
// +------------------------------------------------------------------------------------------------+

static uint8_t get_workout_data_field_size(uint8_t field_id)
{
    switch (field_id)
    {
        case WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_HEARTRATE:
            return 1;
        case WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_LEVEL:
            return 1;
        case WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_SPEED:
        case WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_PACE:
        case WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ENHANCED_INST_SPEED:
        case WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_HORIZONTAL_DISTANCE:
            return 4;
        case WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_METS:
            return 1;
        case WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_GEAR:
            return 1;
        default:
            return 2;
    }
}


static void process_workout_data_update(void)
{
    uint8_t update_data[50];
    uint8_t update_data_offset = 0;

    for (uint8_t field_ind = 0; field_ind < ARRAY_LENGTH(_workout_data_field_set); field_ind++)
    {
        if (_workout_data_field_set[field_ind])
        {
            update_data[update_data_offset++] = field_ind;
            uint8_t field_size = get_workout_data_field_size(field_ind);
            uint32_t field_val = _workout_data_field_val[field_ind];
            /*	if (field_size > 0) */	update_data[update_data_offset++] = (uint8_t)  field_val;
            if (field_size > 1){	update_data[update_data_offset++] = (uint8_t)( field_val >> 8 );	}
            if (field_size > 2){	update_data[update_data_offset++] = (uint8_t)( field_val >> 16 );
                update_data[update_data_offset++] = (uint8_t)( field_val >> 24 );	}
        }
    }
    if (update_data_offset > 0)
    {
        send_cmd_msg(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT, WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_UPDATE_WORKOUT_DATA, update_data_offset, update_data, WF_GEM_HCI_DEFAULT_MAX_COMMAND_RETRY_ATTEMPTS, WF_GEM_HCI_DEFAULT_COMMAND_TIMEOUT_MS);
    }
}


// +------------------------------------------------------------------------------------------------+
// |																								|
// |							Incoming Command Response Handlers									|
// |																								|
// +------------------------------------------------------------------------------------------------+


static void handle_cmd_resp_get_fe_type(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint8_t type_val = message->data[0];
    wf_gem_hci_gymconnect_fitness_equipment_type_e type = WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_UNKNOWN;
    if (type_val <= (uint8_t)WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_ROWER)
    {
        type = (wf_gem_hci_gymconnect_fitness_equipment_type_e)type_val;
    }
    wf_gem_hci_manager_gymconnect_on_command_response_get_fe_type(type);
}

static void handle_cmd_resp_set_fe_type(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        wf_gem_hci_manager_gymconnect_on_command_response_set_fe_type(WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_UNKNOWN_ERROR);
        return;
    }
    wf_gem_hci_manager_gymconnect_on_command_response_set_fe_type(message->data[0]);
}

static void handle_cmd_resp_get_fe_state(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        return;
    }
    uint8_t state_val = message->data[0];
    wf_gem_hci_gymconnect_fitness_equipment_state_e state = WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_UNKNOWN;
    if (state_val <= (uint8_t)WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_FINISHED)
    {
        state = (wf_gem_hci_gymconnect_fitness_equipment_state_e)state_val;
    }
    wf_gem_hci_manager_gymconnect_on_command_response_get_fe_state(state);
}

static void handle_cmd_resp_set_fe_state(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        wf_gem_hci_manager_gymconnect_on_command_response_set_fe_state(WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_UNKNOWN_ERROR);
        return;
    }
    wf_gem_hci_manager_gymconnect_on_command_response_set_fe_state(message->data[0]);
}

static void handle_cmd_resp_get_fe_workout_prog_name(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        wf_gem_hci_manager_gymconnect_on_command_response_get_fe_program_name("");
    }
    else if (message->data[message->data_length - 1] == 0)
    {
        wf_gem_hci_manager_gymconnect_on_command_response_get_fe_program_name((utf8_data_t*)message->data);
    }
}

static void handle_cmd_resp_set_fe_workout_prog_name(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        wf_gem_hci_manager_gymconnect_on_command_response_set_fe_program_name(WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_UNKNOWN_ERROR);
        return;
    }
    wf_gem_hci_manager_gymconnect_on_command_response_set_fe_program_name(message->data[0]);
}


static void handle_cmd_resp_update_workout_data(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length == 0)
    {
        // unexpected data length...
        wf_gem_hci_manager_gymconnect_on_workout_data_update_complete(WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_UNKNOWN_ERROR);
        return;
    }
    wf_gem_hci_manager_gymconnect_on_workout_data_update_complete(message->data[0]);
}


static void handle_event_heart_rate_value_received(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length != 5) 
    {
        // unexpected data length...
        return;
    }
    uint16_t hr_val = (uint16_t)((uint16_t)message->data[0] | (uint16_t)(message->data[1] << 8));
    uint16_t avg_hr_val = (uint16_t)((uint16_t)message->data[2] | (uint16_t)(message->data[3] << 8));
    uint8_t source = message->data[4];
    wf_gem_hci_manager_gymconnecton_event_heart_rate_value_received(hr_val, avg_hr_val, source);
}
static void handle_event_calorie_value_received(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length != 7) 
    {
        // unexpected data length...
        return;
    }

    uint16_t accumulated_total_calories = (uint16_t)((uint16_t)message->data[0] | (uint16_t)(message->data[1] << 8));
    uint16_t accumulated_active_calories = (uint16_t)((uint16_t)message->data[2] | (uint16_t)(message->data[3] << 8));
    uint16_t current_calorie_rate = (uint16_t)((uint16_t)message->data[4] | (uint16_t)(message->data[5] << 8));
    uint8_t calorie_source = message->data[6];

    wf_gem_hci_manager_gymconnecton_event_calorie_value_received(accumulated_total_calories,accumulated_active_calories, current_calorie_rate, calorie_source);
}

static void handle_event_user_information_received(wf_gem_hci_comms_message_t* message)
{
    // TO IMPLEMENT
}




static void handle_event_cadence_value_received(wf_gem_hci_comms_message_t* message)
{
    if (message->data_length != 2)
    {
        // unexpected data length...
        return;
    }
    uint16_t cadence_val = message->data[0];
    cadence_val |= message->data[1] << 8;
    wf_gem_hci_manager_gymconnecton_event_cadence_value_received(cadence_val);
}



typedef void (*function_pointer_cmd_handler_t)(wf_gem_hci_comms_message_t* message);

static const function_pointer_cmd_handler_t CMD_HANDLERS_0[] = {
    NULL,
    handle_cmd_resp_get_fe_type,                //WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_TYPE
    handle_cmd_resp_set_fe_type,                //WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TYPE
    handle_cmd_resp_get_fe_state,               //WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_STATE
    handle_cmd_resp_set_fe_state,               //WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_STATE
    handle_cmd_resp_get_fe_workout_prog_name,   //WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_WORKOUT_PROG_NAME
    handle_cmd_resp_set_fe_workout_prog_name,   //WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_WORKOUT_PROG_NAME
};

static const uint8_t CMD_HANDLERS_1_INDEX_OFFSET = 0x23;
static const function_pointer_cmd_handler_t CMD_HANDLERS_1[] = {
    handle_cmd_resp_update_workout_data         //WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_UPDATE_WORKOUT_DATA
};


static const function_pointer_cmd_handler_t EVT_HANDLERS[] = {
    NULL,
    handle_event_heart_rate_value_received,
    handle_event_cadence_value_received,
    handle_event_user_information_received,
    handle_event_calorie_value_received
};




#define CALL_CMD_HANDLER(MSG_ID, CMD_HANDLERS_ARRAY_NAME)			\
	do {															\
		if (MSG_ID < ARRAY_LENGTH(CMD_HANDLERS_ARRAY_NAME))			\
		{															\
			if (CMD_HANDLERS_ARRAY_NAME[MSG_ID]) 					\
			{														\
				CMD_HANDLERS_ARRAY_NAME[MSG_ID](message);			\
				return;												\
			}														\
		}															\
	} while (0)

#define CALL_CMD_HANDLER_WITH_INDEX_OFFSET(MSG_ID, CMD_HANDLERS_ARRAY_NAME, INDEX_OFFSET)							\
	do {																											\
		if (MSG_ID >= INDEX_OFFSET && ((MSG_ID) - (INDEX_OFFSET)) < ARRAY_LENGTH(CMD_HANDLERS_ARRAY_NAME))			\
		{																											\
			if (CMD_HANDLERS_ARRAY_NAME[((MSG_ID) - (INDEX_OFFSET))]) 												\
			{																										\
				CMD_HANDLERS_ARRAY_NAME[((MSG_ID) - (INDEX_OFFSET))](message);										\
				return;																								\
			}																										\
		}																											\
	} while (0)



static void handle_incoming_message(wf_gem_hci_comms_message_t* message)
{
    if (message->message_class_id != WF_GEM_HCI_MSG_CLASS_GYM_CONNECT)
    {
        // this message isn't meant for us
        return;
    }
    
    if (message->message_event_flag)
    {
        CALL_CMD_HANDLER					(message->message_id, EVT_HANDLERS);
        return;
    }
    
    CALL_CMD_HANDLER					(message->message_id, CMD_HANDLERS_0);
    CALL_CMD_HANDLER_WITH_INDEX_OFFSET	(message->message_id, CMD_HANDLERS_1, CMD_HANDLERS_1_INDEX_OFFSET);
}


#endif	// #if WF_GEM_HCI_CONFIG_INCLUDE_GYMCONNECT_MANAGER

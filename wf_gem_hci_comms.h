//  Copyright (c) 2012-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//


#ifndef _WF_GEM_HCI_COMMS_H_
#define _WF_GEM_HCI_COMMS_H_


#include <stdint.h>
#include <stdbool.h>

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Public Defines, Constants, Types
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

#define WF_GEM_HCI_COMMS_MESSAGE_DATA_LEN_MAX       64



typedef struct
{
    bool        message_event_flag;
    uint8_t     message_class_id;
    uint8_t     message_id;
    uint8_t     data_length;
    uint8_t     data[WF_GEM_HCI_COMMS_MESSAGE_DATA_LEN_MAX];
}
wf_gem_hci_comms_message_t;


typedef enum
{
    WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_NONE                     = 0,
    WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS                  = WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_NONE,
    WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_INVALID_DATA_LENGTH      = 200,
    WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_INVALID_PARAMETER_VALUE  = 201,
    WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_MALFORMED_UTF8_STRING    = 202,
    WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_INCORRECT_STATE          = 203,
    WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_NOT_SUPPORTED            = 204,
    WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_UNKNOWN_MESSAGE_TYPE     = 254,
    WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_UNKNOWN_ERROR            = 255
}
wf_gem_hci_comms_common_error_code_e;


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// |																			|
// |		Public Variables													|
// |																			|
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// |																			|
// |		Public functions													|
// |																			|
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

// init should only be called once per device powerup.
void wf_gem_hci_comms_init(void);

void wf_gem_hci_comms_start(void);
void wf_gem_hci_comms_stop(void);


// This method will pack the message into a GEM HCI frame and send it by 
// calling the wf_gem_hci_comms_on_send_byte callback for each byte that
// needs to be sent over the UART.
// See wf_gem_hci_comms_on_send_byte below.
void wf_gem_hci_comms_send_message(wf_gem_hci_comms_message_t* message);


// It is intended that this method is called from the UART receive 
// interrupt handler/interrupt service routine (ISR)
// Note: when a valid frame is received, this method will call the 
// wf_gem_hci_comms_on_message_received callback/event handler
void wf_gem_hci_comms_process_rx_byte(uint8_t rx_byte);


// if a UART error occurs (UART framing error etc) call this method to 
// reset the frame receiving logic.  Any frame currently being received will be discarded.
void wf_gem_hci_comms_handle_rx_error(void);




// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// |																			|
// |		External Event Handlers												|
// |																			|
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

// All of the methods below must be implemented in the project somewhere.


// When this function is called by wf_gem_hci_comms, the applicaiton should send the 
// byte to the GEM module via the UART TX line.
// The UART should be configured with bps of 115200, 8 data bits, 1 stop bit, no parity.
extern void wf_gem_hci_comms_on_send_byte(uint8_t tx_byte);


// Note that this method is executed from the same context as wf_gem_hci_comms_process_rx_byte.
// The content of the message is only valid within this method call - the handler should copy the
// message structure if needed, or make use of the message immediately.
extern void wf_gem_hci_comms_on_message_received(wf_gem_hci_comms_message_t* message);


/*

The simplest implementation of wf_gem_hci_comms_on_message_received handler is as follows:
-------------------------------------------------------------------------------------------

void wf_gem_hci_comms_on_message_received(wf_gem_hci_comms_message_t* message)
{
	wf_gem_hci_manager_process_recevied_message(message);
}



If wf_gem_hci_comms_process_rx_byte is being called from a UART RX interrupt, this 
means that wf_gem_hci_comms_on_message_received fill be called from the UART RX 
interrupt.  Calling wf_gem_hci_manager_process_recevied_message directly from the 
interrupt service handler "context" might be problematic, depending on the applicaiton
implementation.


To avoid calling wf_gem_hci_manager_process_recevied_message from the interrupt service routine 
context, it is recommended that a first in first out (FIFO) buffer implemented to buffer the 
received message.  Processing of the pending messages can then be performed from outside of the 
interrupt handler/interrupt service routine.


For example, a hypothetical implementation may be as follows:
-------------------------------------------------------------------------------------------


void wf_gem_hci_comms_on_message_received(wf_gem_hci_comms_message_t* message)
{
	// this function must memcopy the data in the message struct
	add_message_to_a_fifo_buffer(message);
	
	schedule_on_main_loop(process_next_rx_message);
}



// run on the main loop by the scheduler / other mechanism etc.
void process_rx_messages(void)
{
	while (true)
	{
		// disable interrupts is one way to ensure that the FIFO buffer
		// is "locked" while we pop from it.
		// Alternatively, some other from of mutex could be used.
		disable_uart_rx_interrupt();
	
		wf_gem_hci_comms_message_t message;
		bool got_message = pop_message_from_fifo_buffer(&message);
	
		enable_uart_rx_interrupt();
	
		if (got_message)
		{
			// there is another message to process
			wf_gem_hci_manager_process_recevied_message(&message);
		}
		else
		{
			// fifo buffer is empty.
			break;
		}
	}
}

*/



#endif

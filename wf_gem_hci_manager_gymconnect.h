//  Copyright (c) 2012-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#ifndef _WF_GEM_HCI_MANAGER_GYMCONNECT_H_
#define _WF_GEM_HCI_MANAGER_GYMCONNECT_H_

#include "wf_gem_hci_config.h"
#if WF_GEM_HCI_CONFIG_INCLUDE_GYMCONNECT_MANAGER

#include "wf_gem_hci_manager.h"



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Public Defines, Constants, Types
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+'

typedef enum
{
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_TYPE                   = 0x01,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TYPE                   = 0x02,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_STATE                  = 0x03,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_STATE					= 0x04,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_WORKOUT_PROG_NAME		= 0x05,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_WORKOUT_PROG_NAME		= 0x06,

    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_UPDATE_WORKOUT_DATA			= 0x23,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_ADDITIONAL_OPTIONS        = 0x24,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_ADDITIONAL_OPTIONS        = 0x25,

    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_SUPPORTED_UPLOAD_ITEM_TYPES_MESSAGE            = 0x40,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_BEGIN_UPLOAD_RECEIVED_MESSAGE                  = 0x41,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_UPLOAD_CHUNK_RECEIVED_MESSAGE                  = 0x42,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_FINISH_UPLOAD_RECEIVED_MESSAGE                 = 0x43,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_CANCEL_UPLOAD_RECEIVED_MESSAGE                 = 0x44,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_UPLOAD_ERROR_MESSAGE                           = 0x45,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_NOTIFY_UPLOAD_PROCESSING_AND_ISSUES_MESSAGE    = 0x46,

    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_CONTROL_FEATURES                       = 0x4F,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_CONTROL_FEATURES                       = 0x50,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_EQUIPMENT_CONTROL_RECEIVED_MESSAGE     = 0x51,

    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_SPEED_RANGE                            = 0x52,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_SPEED_RANGE                            = 0x53,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_INCLINE_CONTROL_RANGE                  = 0x54,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_INCLINE_CONTROL_RANGE                  = 0x55,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_RESISTANCE_CONTROL_RANGE               = 0x56,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_RESISTANCE_CONTROL_RANGE               = 0x57,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TARGET_POWER_CONTROL_RANGE             = 0x58,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_TARGET_POWER_CONTROL_RANGE             = 0x59,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TARGET_HEART_RATE_CONTROL_RANGE        = 0x5A,
    WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_TARGET_HEART_RATE_CONTROL_RANGE        = 0x5B,

}
wf_gem_hci_command_id_gym_connect_e;

typedef enum
{
	WF_GEM_HCI_EVENT_ID_GYM_CONNECT_HEART_RATE_VALUE_RECEIVED		            = 0x01,
	WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CADENCE_VALUE_RECEIVED			            = 0x02,
	WF_GEM_HCI_EVENT_ID_GYM_CONNECT_USER_INFORMATION_RECEIVED		            = 0x03,
    WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CALORIE_DATA_RECEIVED		                = 0x04,
    WF_GEM_HCI_EVENT_ID_GYM_CONNECT_REQUEST_SUPPORTED_UPLOAD_ITEM_TYPES_EVENT   = 0x40,
    WF_GEM_HCI_EVENT_ID_GYM_CONNECT_BEGIN_UPLOAD_EVENT                          = 0x41,
    WF_GEM_HCI_EVENT_ID_GYM_CONNECT_UPLOAD_CHUNK_EVENT                          = 0x42,
    WF_GEM_HCI_EVENT_ID_GYM_CONNECT_FINISH_UPLOAD_EVENT                         = 0x43,
    WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CANCEL_UPLOAD_EVENT                         = 0x44,
    WF_GEM_HCI_EVENT_ID_GYM_CONNECT_REQUEST_UPLOAD_PROCESSING_AND_ISSUES_EVENT  = 0x45,

    WF_GEM_HCI_EVENT_ID_GYM_CONNECT_EQUIPMENT_CONTROL_EVENT                     = 0x51,

}
wf_gem_hci_event_id_gym_connect_e;




typedef enum
{
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ELAPSED_WORKOUT_TIME                    = 0,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_REMAINING_WORKOUT_TIME                  = 1,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_HEARTRATE                               = 2,  
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_LEVEL                                   = 3,  
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_RESISTANCE                              = 4,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_SPEED                                   = 5,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CADENCE                                 = 6,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MOVEMENTS_COUNT                         = 7,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_HORIZONTAL_DISTANCE          = 8,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_VERTICAL_DISTANCE            = 9,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_VERTICAL_DISTANCE_NEGATIVE   = 10,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_ENERGY                       = 11,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ENERGY_RATE                             = 12,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_METS                                    = 13,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_POWER                                   = 14,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_TORQUE                                  = 15, 
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_GEAR                                    = 16,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_GRADE                                   = 17,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ANGLE                                   = 18,
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_FLOOR_RATE                              = 19, 
	WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMULATIVE_FLOORS                       = 20,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CUMU_LAPS                               = 21,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CURRENT_MOVEMENT_LENGTH                 = 22,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CURRENT_PACE                            = 23,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_PACE                            = 24,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CURRENT_FORCE                           = 25,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_CURRENT_SIGNED_LEVEL                    = 26,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_POWER                           = 27,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_CADENCE                         = 28,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_SPEED                           = 29,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_ENHANCED_INST_SPEED                     = 30,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_REPITIONS                               = 31,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_WEIGHT                                  = 32,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_HR                              = 33,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MAXIMUM_HR                              = 34,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_FLOOR_RATE                      = 35,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_ENERGY_RATE                     = 36,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_AVERAGE_METS                            = 37,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_TEMPERATURE                             = 38,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_SPINDOWN_CONDITIONS                     = 39,
    WF_GEM_HCI_GYM_CONNECT_WORKOUT_DATA_MAX_DATA_FIELDS
}
wf_gem_hci_gym_connect_workout_data_field_id_e;

typedef enum
{
	WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_UNKNOWN                = 0,
	WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_TREADMILL              = 1,
	WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_BIKE                   = 2,
	WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_STEPPER                = 3,
	WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_STEP_MILL              = 4,
	WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_CROSS_TRAINER          = 5,
	WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_TOTAL_BODY_TRAINER     = 6,
	WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_TREAD_CLIMBER          = 7,
	WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_ROWER                  = 8,
}
wf_gem_hci_gymconnect_fitness_equipment_type_e;


typedef enum
{
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_UNKNOWN					            = 0,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IDLE                                  = 1,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_PAUSED                                = 2,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_STOP_SAFETY_KEY                       = 3,

    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE                                = 10,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_WARMING_UP                     = 11,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_LOW_INTENSITY                  = 12,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_HIGH_INTENSITY                 = 13,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_RECOVERY                       = 14,
    /// 15 TO 30 RESERVED
    /// 31 TO 50 IN-USE WORKOUT PROGRAM
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_ISOMETRIC                      = 51,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_HR_CONTROL                     = 52,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_FITNESS_TEST                   = 53,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_COOL_DOWN                      = 54,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_WATT_CONTROL                   = 55,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_MANUAL_QUICK_START             = 56,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_SPEED_CHANGED           = 57,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_INCLINE_CHANGED         = 58,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_RESISTANCE_CHANGED      = 59,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_POWER_CHANGED           = 60,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_HR_CHANGED              = 61,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_ENERGY_CHANGED          = 62,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_NUM_STEPS_CHANGED       = 63,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_NUM_STRIDES_CHANGED     = 64,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_DISTANCE_CHANGED        = 65,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_TRAINING_TIME           = 66,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_TIME_TWO_HR_ZONE        = 67,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_TIME_THREE_HR_ZONE      = 68,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_TIME_FIVE_HR_ZONE       = 69,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_BIKE_SIMULATION_PARAM_CHANGE   = 70,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_SPINDOWN_STATUS                = 71,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_TARGET_CADENCE_CHANGED         = 72,

    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_CONTROL_PERMISSION_REOVKE      = 73,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_RESET                          = 74,

    /// 75 TO 126 RESERVED FOR FTMS STATES

    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE_CUSTOM_STATE_NAME              = 127,
    WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_FINISHED					            = 128
}
wf_gem_hci_gymconnect_fitness_equipment_state_e;


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Public Variables
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Public functions
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+


/**
 Get the Fitness Equipment Type
 */
void wf_gem_hci_manager_gymconnect_get_fe_type(void);

/**
 Set the Fitness Equipment Type

 @param fe_type Equipment Type
 */
void wf_gem_hci_manager_gymconnect_set_fe_type(wf_gem_hci_gymconnect_fitness_equipment_type_e fe_type);

/**
 Get the Fitness Equipment State
 */
void wf_gem_hci_manager_gymconnect_get_fe_state(void);

/**
 Sets the Fitness Equipment State

 @param fe_state Equipment State
 */
void wf_gem_hci_manager_gymconnect_set_fe_state(wf_gem_hci_gymconnect_fitness_equipment_state_e fe_state);

/**
 Get the Fitness Equipment Workout Program Name
 */
void wf_gem_hci_manager_gymconnect_get_fe_workout_program_name(void);

/**
 Set the Fitness Equipment Workout Programe name

 @param workout_program_name UTF8 String
 */
void wf_gem_hci_manager_gymconnect_set_fe_workout_program_name(utf8_data_t* workout_program_name);



void wf_gem_hci_manager_gymconnect_get_supported_equipment_control_features(void);

void wf_gem_hci_manager_gymconnect_set_supported_equipment_control_features(uint32_t equipment_control_field_identifier);


// use the below wf_gem_hci_manager_gymconnect_set_workout_data_***** methods
// to configure the data which will be sent to the GEM module, and then call this
// method.
//
// Calling of appropriate wf_gem_hci_manager_gymconnect_set_workout_data_***** methods 
// followed by calling of wf_gem_hci_manager_gymconnect_perform_workout_data_update should occur
// once every second so that new data is sent to the GEM at 1Hz.
//
// See below for an example.
void wf_gem_hci_manager_gymconnect_perform_workout_data_update(void);


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		Public functions for Workout Data Set/Clear
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

// each data field has a set method and a clear ("clr") method.

// deci_***** -> the value is in tenths.  
// A logical value of 12.3 is represented as a parameter value of 123
//
// centi_***** -> the value is in hundredths.  
// A logical value of 12.34 is represented as a parameter value of 1234

/**
 Sets The Workout Data Value for Elapsed Time. 
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_ELAPSED_WORKOUT_TIME

 @param seconds Time in Seconds
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_elapsed_workout_time(uint16_t seconds);

/**
 Clears the Workout Data for Elapsed Workout Time
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_ELAPSED_WORKOUT_TIME
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_elapsed_workout_time(void);

/**
 Sets The Workout Data Value for Remaining Time
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_REMAINING_WORKOUT_TIME
 @param seconds Time in Seconds
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_remaining_workout_time(uint16_t seconds);

/**
  Clears the Workout Data for Remaining Workout Time
  HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_REMAINING_WORKOUT_TIME
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_remaining_workout_time(void);

/**
 Sets The Workout Data Value for Heart Rate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_HR
 @param beats_per_minute Heart Rate in BPM
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_heartrate(uint8_t beats_per_minute);

/**
 Clears the Workout Data for Heartrate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_HR
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_heartrate(void);

/**
 Sets The Workout Data Value for Average Heart Rate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_HR
 @param beats_per_minute Heart Rate in BPM
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_avg_heartrate(uint8_t beats_per_minute);

/**
 Clears the Workout Data for Average Heartrate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_HR
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_avg_heartrate(void);

/**
 Sets The Workout Data Value for Maximum Heart Rate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_MAXIMUM_HR
 @param beats_per_minute Heart Rate in BPM
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_max_heartrate(uint8_t beats_per_minute);

/**
 Clears the Workout Data for Heartrate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_MAXIMUM_HR
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_max_heartrate(void);




/**
 Sets The Workout Data Value for Level
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_LEVEL
 @param level Level (unitless)
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_level(uint8_t level);

/**
 Clears the Workout Data for Level
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_LEVEL
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_level(void);

/**
 Sets The Workout Data Value for Resistance
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_RESISTANCE_SETTING
 @param deci_resistance Resistance (unitless)
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_resistance(uint16_t deci_resistance);

/**
 Clears the Workout Data for Resistance
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_RESISTANCE_SETTING
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_resistance(void);

/**
 Sets The Workout Data Value for Speed
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_SPEED
 @param centi_kph Speed in KPH
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_speed(uint16_t centi_kph);

/**
 Clears the Workout Data for Speed
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_SPEED
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_speed(void);

/**
 Sets The Workout Data Value for Pace
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_PACE
 @param pace_s_per_km Pace in s/km (except rower where is it seconds per 500m)
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_current_pace(uint16_t pace_s_per_km);

/**
 Clears the Workout Data for Pace
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_PACE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_current_pace(void);

/**
 Sets The Workout Data Value for Average Pace
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_PACE
 @param pace_s_per_km Pace in s/km (except rower where is it seconds per 500m)
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_average_pace(uint32_t pace_ms_per_km);

/**
 Clears the Workout Data for Average Pace
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_PACE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_average_pace(void);

/**
 Sets The Workout Data Value for Cadnece

 Units are "x per minute" where: Bike x = revolutions,  Stair Climber x = steps,
 Stairmaster x = strides, Cross Trainer x = strides, Rower x = strokes
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_CADENCE

 @param deci_cadence_x_per_minute Cadence in x/minute
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_cadence(uint16_t deci_cadence_x_per_minute);

/**
 Clears the Workout Data for Cadence
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_CADENCE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_cadence(void);

/**
 Sets The Workout Data Value for Movements Count

 Cumulative count of steps/strokes etc.
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_MOVEMENTS

 @param movements_count Movement Count
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_movements_count(uint16_t movements_count);

/**
  Clears the Workout Data for Movements Count
  HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_MOVEMENTS
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_movements_count(void);

/**
 Sets The Workout Data Value for Cumulative Horizontal Distance
HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_HORIZ_DIST
 @param meters Cumulative Horizontal Distance in Meters
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_horizontal_distance(uint32_t meters);

/**
 Clears the Workout Data for Cumulative Horizontal Distance
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_HORIZ_DIST
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_horizontal_distance(void);

/**
 Sets the Workout Data for Cumulative Vertical Distance
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_VERT_DIST
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_vertical_distance(uint16_t deci_meters);

/**
 Clears the Workout Data for Cumulative Vertical Distance
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_VERT_DIST
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_vertical_distance(void);

/**
  Sets the Workout Data for Cumulative Negative Vertical Distance
  HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_VERT_DIST_NEGATIVE
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_vertical_distance_negative(uint16_t deci_meters);

/**
 Clears the Workout Data for Cumulative Negative Vertical Distance
  HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_VERT_DIST_NEGATIVE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_vertical_distance_negative(void);

/**
 Sets the Workout Data for Cumulative Energy
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_ENERGY
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_energy(uint16_t kilogram_calories);

/**
 Clears the Workout Data for Cumulative Energy
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_ENERGY
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_energy(void);

/**
 Sets the Workout Data for Energy Rate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_ENERGY_RATE
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_energy_rate(uint16_t kilogram_calories_per_hour);

/**
 Clears the Workout Data for Energy Rate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_ENERGY_RATE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_energy_rate(void);

/**
 Sets the Workout Data for METs
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_METS
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_mets(uint8_t deci_mets);

/**
 Clears the Workout Data for METs
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_METS
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_mets(void);

/**
 Sets the Workout Data for Power
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_POWER
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_power(int16_t watts);

/**
 Clears the Workout Data for Power
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_POWER
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_power(void);

/**
 Sets the Workout Data for Torque
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_TORQUE
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_torque(uint16_t deci_newton_meters);

/**
 Clears the Workout Data for Torque
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_TORQUE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_torque(void);

/**
 Sets the Workout Data for Gear
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_GEAR
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_gear(uint8_t gear_number);

/**
 Clears the Workout Data for Gear
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_GEAR
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_gear(void);

/**
 Sets the Workout Data for Grade
 0% is flat, 100% is 45 degrees uphill, -100% is 45 degrees downhill
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_GRADE
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_grade(int16_t deci_grade);

/**
 Clears the Workout Data for Grade
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_GRADE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_grade(void);

/**
 Sets the Workout Data for Angle
 0 degrees is flat, 90 degrees is straight up, -90 degrees is straight down
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_ANGLE
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_angle(int16_t centi_degrees);

/**
 Clears the Workout Data for Angle
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_ANGLE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_angle(void);

/**
 Sets the Workout Data for Floor Rate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_FLOOR_RATE
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_floor_rate(uint16_t centi_floors_per_minute);

/**
 Clears the Workout Data for Floor Rate
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_FLOOR_RATE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_floor_rate(void);

/**
 Sets the Workout Data for Cumulative Floors
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_FLOORS
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_floors(uint16_t centi_floors);

/**
 Clears the Workout Data for Cumulative Floors
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_CUMU_FLOORS
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_cumulative_floors(void);

//  HCI_GYM_CONNECT_MEAS_FIELD_CUMU_LAPS
//  HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_MOVEMENT_LENGTH
//  HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_PACE
//  HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_PACE
//  HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_FORCE
//  HCI_GYM_CONNECT_MEAS_FIELD_CURRENT_SIGNED_LEVEL
//  
/**
 Sets the Workout Data for Average Power
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_POWER
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_average_power(int16_t power);

/**
 Clears the Workout Data for Average Power
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_POWER
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_average_power(void);

/**
 Sets the Workout Data for Average Cadence
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_CADENCE
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_average_cadence(uint16_t average_cadence);

/**
 Clears the Workout Data for Average Cadence
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_CADENCE
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_average_cadence(void);

/**
 Sets the Workout Data for Average Speed
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_SPEED
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_average_speed(uint32_t average_speed);

/**
 Clears the Workout Data for Average Speed
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_SPEED
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_average_speed(void);

//  HCI_GYM_CONNECT_MEAS_FIELD_ENHANCED_INST_SPEED
/**
 Sets the Workout Data for Enhanced Speed
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_ENHANCED_INST_SPEED
 */
void wf_gem_hci_manager_gymconnect_set_workout_data_enhanced_speed(uint32_t enhanced_speed);

/**
 Clears the Workout Data for Enhanced Speed
 HCI Workout Data Enum: HCI_GYM_CONNECT_MEAS_FIELD_ENHANCED_INST_SPEED
 */
void wf_gem_hci_manager_gymconnect_clr_workout_data_enhanced_speed(void);
//  HCI_GYM_CONNECT_MEAS_FIELD_REPITIONS
//  HCI_GYM_CONNECT_MEAS_FIELD_WEIGHT
//  HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_HR
//  HCI_GYM_CONNECT_MEAS_FIELD_MAXIMUM_HR
//  HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_FLOOR_RATE
//  HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_ENERGY_RATE
//  HCI_GYM_CONNECT_MEAS_FIELD_AVERAGE_METS
//  HCI_GYM_CONNECT_MEAS_FIELD_TEMPERATURE
//  HCI_GYM_CONNECT_MEAS_FIELD_SPINDOWN_CONDITIONS



#ifndef SWIG // Ignore for Java wrapper generation
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		External Event Handlers
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

/**
 Event on a Command Send Failure

 @param message HCI Message
 */
extern void wf_gem_hci_manager_gymconnect_on_command_send_failure(wf_gem_hci_comms_message_t* message);

/**
 Event on Command to Get the Fitness Equipment Type

 @param fe_type Equipment Type
 */
extern void wf_gem_hci_manager_gymconnect_on_command_response_get_fe_type(wf_gem_hci_gymconnect_fitness_equipment_type_e fe_type);

/**
 Event on Command to Set Fitness Equpment Type

 @param error_code WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS if Success otherwise Error Code
 */
extern void wf_gem_hci_manager_gymconnect_on_command_response_set_fe_type(uint8_t error_code);

/**
 Event on Command to Get Fitness Equipment State

 @param fe_state Fitness Eqiupment State
 */
extern void wf_gem_hci_manager_gymconnect_on_command_response_get_fe_state(wf_gem_hci_gymconnect_fitness_equipment_state_e fe_state);

/**
 Event on Command to Set Fitness Equipment State

 @param error_code WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS if Success otherwise Error Code
 */
extern void wf_gem_hci_manager_gymconnect_on_command_response_set_fe_state(uint8_t error_code);

/**
 Event on Command to Get Fitness Equipment Program Name

 @param program_name UTF8 String
 */
extern void wf_gem_hci_manager_gymconnect_on_command_response_get_fe_program_name(utf8_data_t* program_name);

/**
 Event on Command to Set Fitness Equipment Program Name

 @param error_code WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS if Success otherwise Error Code
 */
extern void wf_gem_hci_manager_gymconnect_on_command_response_set_fe_program_name(uint8_t error_code);

/**
 Event on Workout Data Update Complete

 @param error_code WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS if Success otherwise Error Code
 */
extern void wf_gem_hci_manager_gymconnect_on_workout_data_update_complete(uint8_t error_code);

/**
 Event on Heart Rate Value Received

 @param heart_rate_value Heart Rate
 */
extern void wf_gem_hci_manager_gymconnecton_event_heart_rate_value_received(uint16_t heart_rate_value, uint16_t avg_heart_rate_value, uint8_t source);

/**
 Event on Calorie Value Received

 @param heart_rate_value Heart Rate
 */
extern void wf_gem_hci_manager_gymconnecton_event_calorie_value_received(uint16_t accum_total_calories, uint16_t accum_average_calories, uint16_t current_calorie_rate, uint8_t source);
/**
 Event on Cadence Value Received

 @param cadence_value Cadence
 */
extern void wf_gem_hci_manager_gymconnecton_event_cadence_value_received(uint16_t cadence_value);


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+



/*

 The following is an example of how the wf_gem_hci_manager_gymconnect_set_workout_data_***** methods
 and the wf_gem_hci_manager_gymconnect_perform_workout_data_update should be used to send data to the GEM

 The example assumes various variables (_workout_time_seconds, _heartrate_bpm etc) exist
 and are being updated elsewhere as required.

 The example only uses some of the possible workout data fields.  For all possible workout data feilds,
 See the above "Public functions for Workout Data Set/Clear" documentation.


 // call this method at 1Hz
 void send_workout_data_to_gem(void)
 {
    wf_gem_hci_manager_gymconnect_set_workout_data_elapsed_workout_time(_workout_time_seconds);
    wf_gem_hci_manager_gymconnect_set_workout_data_heartrate(_heartrate_bpm);
    wf_gem_hci_manager_gymconnect_set_workout_data_level(_curent_level);
    wf_gem_hci_manager_gymconnect_set_workout_data_speed(_speed_centi_kph);
    wf_gem_hci_manager_gymconnect_set_workout_data_cadence(_deci_cadence);
    wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_horizontal_distance(_distance_metres);
    wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_energy(_total__kcals);
    wf_gem_hci_manager_gymconnect_set_workout_data_energy_rate(_kcals_per_hour);
    wf_gem_hci_manager_gymconnect_set_workout_data_mets(_mets);
    wf_gem_hci_manager_gymconnect_set_workout_data_power(_power_watts);
    wf_gem_hci_manager_gymconnect_set_workout_data_torque(_torque_deci_newton_meters);
    wf_gem_hci_manager_gymconnect_set_workout_data_gear(_gear);

    wf_gem_hci_manager_gymconnect_perform_workout_data_update();
    // wf_gem_hci_manager_gymconnect_on_workout_data_update_complete handler will be called
    // upon successful completion of the data update
 }

 void wf_gem_hci_manager_gymconnect_on_workout_data_update_complete(uint8_t error_code)
 {
     if (error_code != WF_GEM_HCI_COMMS_COMMON_ERROR_CODE_SUCCESS) {
     // Deal with error
     }
 }

 */

#endif	// #ifndef SWIG

#endif	//#if WF_GEM_HCI_CONFIG_INCLUDE_GYMCONNECT_MANAGER

#endif 

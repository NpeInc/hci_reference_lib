//  Copyright (c) 2012-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

 
#include "wf_gem_hci_config.h"
#if defined(WF_GEM_HCI_CONFIG_INCLUDE_BOOTLOADER_MANAGER)

 
#include "wf_gem_hci_manager_bootloader.h"

#include <stdlib.h>
#include <string.h>

// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Macros and Definitions										|
// |																								|
// +------------------------------------------------------------------------------------------------+


#define ARRAY_LENGTH(ARRAY_NAME)    (sizeof(ARRAY_NAME) / sizeof(*ARRAY_NAME))
	

#define WF_GEM_HCI_MSG_CLASS_BOOTLOADER                 0x00

#define UPDATER_DATA_BYTES_PER_IMAGE_TRANSFER_MESSAGE   WF_GEM_HCI_COMMS_MESSAGE_DATA_LEN_MAX

#define WAHOO_WB3_UPDATE_IMAGE_TYPE                     1

#define START_BOOTLOADER_COMMAND_INTERVAL_MILLISECONDS  10

// The bootloader is not enforcing its own throttling but instead replying to HCI messages
//   immediately after it gets them.  If transfer goes too quickly, it can fail.  This value
//   forces a minimum delay between an HCI response and the send of the next chunk of image data.
#define IMAGE_TRANSFER_FORCED_DELAY_MILLISECONDS        2


typedef enum
{
    UARTBL_COMMAND_ID_START_BOOTLOADER                  = 0x01,
    UARTBL_COMMAND_ID_EXIT_BOOTLOADER                   = 0x02,
    UARTBL_COMMAND_ID_GET_BOOTLOADER_VERSION_INFO       = 0x03,
    UARTBL_COMMAND_ID_START_IMAGE_TRANSFER              = 0x20,
    UARTBL_COMMAND_ID_TRANSFER_IMAGE_DATA               = 0x40,
    UARTBL_COMMAND_ID_FINALISE_IMAGE_TRANSFER           = 0x50,
}
uartbl_command_id_e;

typedef enum
{
    UARTBL_EVENT_ID_READY_FOR_IMAGE_DATA    = 0x20,
    UARTBL_EVENT_ID_ACTIVATING_IMAGE        = 0x30,
    UARTBL_EVENT_ID_FAILURE                 = 0xF0,
}
uartbl_event_id_e;


typedef enum
{
    UPDATER_STATE_NULL,
    UPDATER_STATE_ENTER_BL,
    UPDATER_STATE_START_IMAGE_TRANSFER,
    UPDATER_STATE_WAIT_FOR_BOOTLOADER_READY_FOR_IMAGE_TRANSFER,
    UPDATER_STATE_SEND_IMAGE_DATA,
    UPDATER_STATE_FINALISE_IMAGE_TRANSFER,
    UPDATER_STATE_WAIT_FOR_IMAGE_ACTIVATE,
    UPDATER_STATE_IMAGE_ACTIVATE_DELAY,
    
    UPDATER_STATE_FAILURE,
    
    UPDATER_STATE_EXIT_BL,
    
}
updater_state_e;




// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Public Variables											|
// |																								|
// +------------------------------------------------------------------------------------------------+


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Private Variables											|
// |																								|
// +------------------------------------------------------------------------------------------------+

// _updater_update_image_size includes 2 extra bytes for CRC
static uint32_t _updater_update_image_size;
static uint32_t _updater_next_image_data_byte_index;

static uint16_t _updater_update_image_crc;

static uint16_t _updater_start_bootloader_attempts_remaining;

static uint8_t _updater_start_image_transfer_attempts;

// updatersm_ prefix = Updater State Machine
static updater_state_e _updatersm_current_state = UPDATER_STATE_NULL;
static updater_state_e _updatersm_new_state = UPDATER_STATE_NULL;

static bool _updatersm_timing_event_scheduled = false;

static bool _updatersm_image_throttling_delay_in_effect = false;

// +------------------------------------------------------------------------------------------------+
// |																								|
// |							Private Function Declarations										|
// |																								|
// +------------------------------------------------------------------------------------------------+

#define send_cmd_msg    _wf_gem_hci_manager_send_command_message
void _wf_gem_hci_manager_send_command_message(uint8_t message_class_id, uint8_t message_id, uint8_t data_length, const uint8_t* const data, uint8_t max_retries, uint16_t cmd_timeout_ms);
void _wf_gem_hci_manager_cancel_outstanding_command_message(void);


static void send_cmd_start_bootloader(uint8_t auto_retries, uint16_t cmd_timeout_ms);
static void send_cmd_exit_bootloader(uint8_t auto_retries, uint16_t cmd_timeout_ms);
static void send_cmd_start_image_transfer(uint16_t image_type, uint32_t image_size, uint8_t auto_retries, uint16_t cmd_timeout_ms);
static void send_cmd_transfer_image_data(const uint8_t* const data, uint8_t data_length, uint8_t auto_retries, uint16_t cmd_timeout_ms);
static void send_cmd_finalise_image_transfer(uint16_t image_crc, uint8_t auto_retries, uint16_t cmd_timeout_ms);

static void handle_incoming_message(wf_gem_hci_comms_message_t* message);
static void handle_command_timeout(wf_gem_hci_comms_message_t* message);


// updatersm_ prefix = Updater State Machine
static void updatersm_schedule_timing_event(uint16_t delay_milliseconds);
static void updatersm_change_state(updater_state_e new_state);
static void updatersm_process(bool enter, bool timing_event, wf_gem_hci_comms_message_t* message, bool command_timeout);


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Public Functions											|
// |																								|
// +------------------------------------------------------------------------------------------------+

void wf_gem_hci_manager_bootloader_begin_update_process(uint16_t start_bootloader_timeout_ms, uint32_t update_image_size)
{
    if (update_image_size <= 2)
    {
        updatersm_change_state(UPDATER_STATE_FAILURE);
        return;
    }
    // _updater_update_image_size includes 2 extra bytes for CRC
    _updater_update_image_size = update_image_size;

    _updater_start_bootloader_attempts_remaining = start_bootloader_timeout_ms / START_BOOTLOADER_COMMAND_INTERVAL_MILLISECONDS;
    updatersm_change_state(UPDATER_STATE_ENTER_BL);
}

void wf_gem_hci_manager_bootloader_cancel_update_process(bool exit_bootloader)
{
    if (exit_bootloader)
    {
        updatersm_change_state(UPDATER_STATE_EXIT_BL);
    }
    else
    {
        updatersm_change_state(UPDATER_STATE_NULL);
    }
}


void wf_gem_hci_manager_bootloader_send_update_image_data(const uint8_t* const data, uint8_t num_data_bytes)
{
    // note: _updater_update_image_size includes 2 extra bytes for CRC
    if (_updater_next_image_data_byte_index + num_data_bytes > _updater_update_image_size)
    {
        // this is an error
        updatersm_change_state(UPDATER_STATE_FAILURE);
        return;
    }


    wf_gem_hci_manager_bootloader_on_cancel_update_process_timer();

    // note: num_image_data_bytes will be adjusted to be the number of image bytes to send now EXCLUDING
    // any CRC bytes that the consumer has passed in the data buffer
    int16_t num_image_data_bytes = num_data_bytes;

    int32_t num_crc_bytes_in_data_buf = (_updater_next_image_data_byte_index + num_data_bytes) - (_updater_update_image_size - 2);
    if (num_crc_bytes_in_data_buf > 0)
    {
        // the data buffer contains at least one byte of the CRC value
        num_image_data_bytes -= num_crc_bytes_in_data_buf;

        uint8_t const * next_crc_data_byte = data + num_image_data_bytes;
        while (num_crc_bytes_in_data_buf > 0)
        {
            _updater_update_image_crc = _updater_update_image_crc >> 8;				// if there's already data in the high byte, shift it to the low byte (because of little endian)
            _updater_update_image_crc |= ((uint16_t)(*next_crc_data_byte) << 8);	// put the new data in the high byte of the uint16
            next_crc_data_byte++;
            num_crc_bytes_in_data_buf--;
        }
    }

    _updater_next_image_data_byte_index += num_data_bytes;


    if (num_image_data_bytes > 0)	// this might be 0 now due to above adjustment
    {
        send_cmd_transfer_image_data(data, num_image_data_bytes, 3, 200);
    }
    else
    {
        // we want to process the next state soon
        // ask the host app to call us back ASAP
        updatersm_schedule_timing_event(0);
    }
}


void wf_gem_hci_manager_bootloader_handle_update_process_timer_fired(void)
{
    if (!_updatersm_timing_event_scheduled)
    {
        return;
    }
    _updatersm_timing_event_scheduled = false;
    // timing event or new state entry
    updatersm_process(false, true, NULL, false);
}


// +------------------------------------------------------------------------------------------------+
// |																								|
// |								Protected Functions												|
// |																								|
// +------------------------------------------------------------------------------------------------+

void _wf_gem_hci_manager_bootloader_handle_incoming_message(wf_gem_hci_comms_message_t* message)
{
    handle_incoming_message(message);
}

void _wf_gem_hci_manager_bootloader_on_command_send_failure_internal(wf_gem_hci_comms_message_t* message)
{
    (void)message;	// unused
    handle_command_timeout(message);
}

// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Private Functions											|
// |																								|
// +------------------------------------------------------------------------------------------------+



// +------------------------------------------------------------------------------------------------+
// |																								|
// |								Outgoing Command Senders										|
// |																								|
// +------------------------------------------------------------------------------------------------+

static void send_cmd_start_bootloader(uint8_t auto_retries, uint16_t cmd_timeout_ms)
{
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_BOOTLOADER, UARTBL_COMMAND_ID_START_BOOTLOADER, 0, NULL, auto_retries, cmd_timeout_ms);
}

static void send_cmd_exit_bootloader(uint8_t auto_retries, uint16_t cmd_timeout_ms)
{
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_BOOTLOADER, UARTBL_COMMAND_ID_EXIT_BOOTLOADER, 0, NULL, auto_retries, cmd_timeout_ms);
}

static void send_cmd_start_image_transfer(uint16_t image_type, uint32_t image_size, uint8_t auto_retries, uint16_t cmd_timeout_ms)
{
    uint8_t data[6];
    uint8_t data_index = 0;
    data[data_index++] = (uint8_t)(image_type      );
    data[data_index++] = (uint8_t)(image_type >>  8);
    data[data_index++] = (uint8_t)(image_size      );
    data[data_index++] = (uint8_t)(image_size >>  8);
    data[data_index++] = (uint8_t)(image_size >> 16);
    data[data_index++] = (uint8_t)(image_size >> 24);
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_BOOTLOADER, UARTBL_COMMAND_ID_START_IMAGE_TRANSFER, sizeof(data), data, auto_retries, cmd_timeout_ms);
}


static void send_cmd_transfer_image_data(const uint8_t* const data, uint8_t data_length, uint8_t auto_retries, uint16_t cmd_timeout_ms)
{
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_BOOTLOADER, UARTBL_COMMAND_ID_TRANSFER_IMAGE_DATA, data_length, data, auto_retries, cmd_timeout_ms);
}


static void send_cmd_finalise_image_transfer(uint16_t image_crc, uint8_t auto_retries, uint16_t cmd_timeout_ms)
{
    uint8_t data[2];
    uint8_t data_index = 0;
    data[data_index++] = (uint8_t)(image_crc      );
    data[data_index++] = (uint8_t)(image_crc >>  8);
    send_cmd_msg(WF_GEM_HCI_MSG_CLASS_BOOTLOADER, UARTBL_COMMAND_ID_FINALISE_IMAGE_TRANSFER, sizeof(data), data, auto_retries, cmd_timeout_ms);
}





// +------------------------------------------------------------------------------------------------+
// |																								|
// |							Incoming Event Handlers												|
// |																								|
// +------------------------------------------------------------------------------------------------+


static void handle_incoming_message(wf_gem_hci_comms_message_t* message)
{
    if (message->message_class_id != WF_GEM_HCI_MSG_CLASS_BOOTLOADER)
    {
        // this message isn't meant for us
        return;
    }

    updatersm_process(false, false, message, false);
}


static void handle_command_timeout(wf_gem_hci_comms_message_t* message)
{
    (void)message;	// unused
    updatersm_process(false, false, NULL, true);
}


// +------------------------------------------------------------------------------------------------+
// |																								|
// |									Updater State Machine										|
// |																								|
// +------------------------------------------------------------------------------------------------+

static void updatersm_schedule_timing_event(uint16_t delay_milliseconds)
{
    if (_updatersm_timing_event_scheduled)
    {
        wf_gem_hci_manager_bootloader_on_cancel_update_process_timer();
    }
    wf_gem_hci_manager_bootloader_on_begin_update_process_timer(delay_milliseconds);
    _updatersm_timing_event_scheduled = true;
}


static void updatersm_change_state(updater_state_e new_state)
{
    wf_gem_hci_manager_bootloader_on_cancel_update_process_timer();
    _wf_gem_hci_manager_cancel_outstanding_command_message();
    _updatersm_new_state = new_state;
    if (_updatersm_new_state == UPDATER_STATE_NULL)
    {
        // we are done now
        _updatersm_current_state = UPDATER_STATE_NULL;
    }
    else
    {
        // we want to process the next state soon
        // ask the host app to call us back ASAP
        updatersm_schedule_timing_event(0);
    }
}







static void updatersm_process(bool enter, bool timing_event, wf_gem_hci_comms_message_t* message, bool command_timeout)
{
    if (_updatersm_new_state != UPDATER_STATE_NULL)
    {
        // we never have a timing event at the same time as new state entry
        timing_event = false;

        _updatersm_current_state = _updatersm_new_state;
        _updatersm_new_state = UPDATER_STATE_NULL;

        // recurse once here to call with enter = true
        updatersm_process(true, false, NULL, false);
    }

    if ( !( enter || timing_event || message != NULL || command_timeout ) )
    {
        // nothing to do?
        return;
    }

    switch (_updatersm_current_state)
    {
        case UPDATER_STATE_NULL:
        {
        }
        break;

        case UPDATER_STATE_ENTER_BL:
        {
            bool new_cmd_send = false;
            if (enter)
            {
                wf_gem_hci_manager_bootloader_on_update_progress_changed(WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_STARTING_BOOTLOADER, 0);
                new_cmd_send = true;
            }
            else if (timing_event)
            {
            }
            else if (message && !message->message_event_flag)
            {
                // this is a response to the command we sent
                _updater_start_image_transfer_attempts = 0;
                updatersm_change_state(UPDATER_STATE_START_IMAGE_TRANSFER);
            }
            else if (command_timeout)
            {
                if (_updater_start_bootloader_attempts_remaining == 0)
                {
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
                else
                {
                    new_cmd_send = true;
                }
            }
            if (new_cmd_send)
            {
                uint8_t retries_now = 200;
                if (retries_now > _updater_start_bootloader_attempts_remaining) retries_now = _updater_start_bootloader_attempts_remaining;
                _updater_start_bootloader_attempts_remaining -= retries_now;
                send_cmd_start_bootloader(retries_now, START_BOOTLOADER_COMMAND_INTERVAL_MILLISECONDS);
            }
        }
        break;

        case UPDATER_STATE_START_IMAGE_TRANSFER:
        {
            if (enter)
            {
                if (_updater_start_image_transfer_attempts >= 3)
                {
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
                else
                {
                    if (_updater_start_image_transfer_attempts == 0)
                    {
                        wf_gem_hci_manager_bootloader_on_update_progress_changed(WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_STARTING_UPDATE, 0);
                    }
                    _updater_start_image_transfer_attempts++;
                    _updater_next_image_data_byte_index = 0;
                    _updater_update_image_crc = 0;
                    // _updater_update_image_size includes 2 extra bytes for CRC - we need to subtract 2 now to send
                    // the true image size value to the bootloader
                    send_cmd_start_image_transfer(WAHOO_WB3_UPDATE_IMAGE_TYPE, _updater_update_image_size - 2, 5, 1000);
                }
            }
            else if (timing_event)
            {
            }
            else if (message && !message->message_event_flag)
            {
                // this is a response to the command we sent
                // check if the response has an error code
                if (message->data_length < 1)
                {
                    // response packet invalid data length
                    // re-enter this state - "enter" handling will retry or fail
                    updatersm_change_state(UPDATER_STATE_START_IMAGE_TRANSFER);
                }
                else if (message->data[0] != 0)
                {
                    // error code != 0
                    // re-enter this state - "enter" handling will retry or fail
                    updatersm_change_state(UPDATER_STATE_START_IMAGE_TRANSFER);
                }
                else
                {
                    updatersm_change_state(UPDATER_STATE_WAIT_FOR_BOOTLOADER_READY_FOR_IMAGE_TRANSFER);
                }
            }
            else if (message && message->message_event_flag)
            {
                if (message->message_id == UARTBL_EVENT_ID_FAILURE)
                {
                    // re-enter this state - "enter" handling will retry or fail
                    updatersm_change_state(UPDATER_STATE_START_IMAGE_TRANSFER);
                }
            }
            else if (command_timeout)
            {
                // re-enter this state - "enter" handling will retry or fail
                updatersm_change_state(UPDATER_STATE_START_IMAGE_TRANSFER);
            }
        }
        break;

        case UPDATER_STATE_WAIT_FOR_BOOTLOADER_READY_FOR_IMAGE_TRANSFER:
        {
            if (enter)
            {
                updatersm_schedule_timing_event(5000);
            }
            else if (timing_event)
            {
                updatersm_change_state(UPDATER_STATE_FAILURE);
            }
            else if (message && message->message_event_flag)
            {
                if (message->message_id == UARTBL_EVENT_ID_READY_FOR_IMAGE_DATA)
                {
                    updatersm_change_state(UPDATER_STATE_SEND_IMAGE_DATA);
                }
                else if (message->message_id == UARTBL_EVENT_ID_FAILURE)
                {
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
            }
        }
        break;

        case UPDATER_STATE_SEND_IMAGE_DATA:
        {
            if (enter)
            {
                uint8_t progress_percent = (_updater_next_image_data_byte_index * 100) / _updater_update_image_size;
                wf_gem_hci_manager_bootloader_on_update_progress_changed(WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_SENDING_UPDATE_DATA, progress_percent);

                // this timeout will be canceled when the host app calls wf_gem_hci_manager_bootloader_send_update_image_data
                updatersm_schedule_timing_event(5000);
                wf_gem_hci_manager_bootloader_on_update_image_data_required( (_updater_next_image_data_byte_index == 0) , UPDATER_DATA_BYTES_PER_IMAGE_TRANSFER_MESSAGE);
            }
            else if (timing_event)
            {
                // timing event is used in two different ways in this state:
                // 1. the main way it is used is to implement a timeout between use dispatching the wf_gem_hci_manager_bootloader_on_update_image_data_required
                // event and the host app calling wf_gem_hci_manager_bootloader_send_update_image_data
                //
                // 2. in some situations we might be waiting for the host app to pass us the image CRC data
                // once all other data has been sent to the bootloader.
                // in this situation this timing event is actually used more like a scheduled callback to signal that
                // the app has finished giving us the CRC value
                //
                if (_updatersm_image_throttling_delay_in_effect)
                {
                    _updatersm_image_throttling_delay_in_effect = false;
                    // Trigger request for next chunk
                    updatersm_change_state(UPDATER_STATE_SEND_IMAGE_DATA);
                }
                else if (_updater_next_image_data_byte_index >= _updater_update_image_size)
                {
                    // this event means that all image data has been sent to the bootloader already,
                    // and now the host app has finihsed sending the CRC bytes to us
                    updatersm_change_state(UPDATER_STATE_FINALISE_IMAGE_TRANSFER);
                }
                else if (_updater_next_image_data_byte_index + 1 == _updater_update_image_size)
                {
                    // this event means that one more byte of the image CRC is required
                    updatersm_change_state(UPDATER_STATE_SEND_IMAGE_DATA);
                }
                else
                {
                    // host app didn't call wf_gem_hci_manager_bootloader_send_update_image_data in time
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
            }
            else if (message && !message->message_event_flag)
            {
                // this is a response to the command we sent in wf_gem_hci_manager_bootloader_send_update_image_data(...) function
                // check if the response has an error code
                if (message->data_length < 1)
                {
                    // response packet invalid data length
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
                else if (message->data[0] != 0)
                {
                    // error code != 0
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
                else
                {
                    // response ok
                    // note: the _updater_next_image_data_byte_index varaible is updated
                    // when the host app calls wf_gem_hci_manager_bootloader_send_update_image_data
                    if (_updater_next_image_data_byte_index >= _updater_update_image_size)
                    {
                        // all bytes have been transferred and the host app has provided the CRC data
                        updatersm_change_state(UPDATER_STATE_FINALISE_IMAGE_TRANSFER);
                    }
                    else
                    {
                        // still more bytes to transfer
                        _updatersm_image_throttling_delay_in_effect = true;
                        updatersm_schedule_timing_event(IMAGE_TRANSFER_FORCED_DELAY_MILLISECONDS);
                    }
                }
            }
            else if (message && message->message_event_flag)
            {
                if (message->message_id == UARTBL_EVENT_ID_FAILURE)
                {
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
            }
            else if (command_timeout)
            {
                updatersm_change_state(UPDATER_STATE_FAILURE);
            }
        }
        break;

        case UPDATER_STATE_FINALISE_IMAGE_TRANSFER:
        {
            if (enter)
            {
                // Throttling delay still applies after final chunk of image data.
                // Otherwise the CRC packet would come in too early and may need to be retried.
                updatersm_schedule_timing_event(IMAGE_TRANSFER_FORCED_DELAY_MILLISECONDS);
            }
            else if (timing_event)
            {
                send_cmd_finalise_image_transfer(_updater_update_image_crc, 3, 1000);

                wf_gem_hci_manager_bootloader_on_update_progress_changed(WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_FINISHING_UPDATE, 0);
            }
            else if (message && !message->message_event_flag)
            {
                // check if the response has an error code
                if (message->data_length < 1)
                {
                    // response packet invalid data length
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
                else if (message->data[0] != 0)
                {
                    // error code != 0
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
                else
                {
                    // response ok
                    updatersm_change_state(UPDATER_STATE_WAIT_FOR_IMAGE_ACTIVATE);
                }
            }
            else if (message && message->message_event_flag)
            {
                if (message->message_id == UARTBL_EVENT_ID_FAILURE)
                {
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
            }
            else if (command_timeout)
            {
                updatersm_change_state(UPDATER_STATE_FAILURE);
            }
        }
        break;

        case UPDATER_STATE_WAIT_FOR_IMAGE_ACTIVATE:
        {
            if (enter)
            {
                updatersm_schedule_timing_event(10000);
            }
            else if (timing_event)
            {
                updatersm_change_state(UPDATER_STATE_FAILURE);
            }
            else if (message && message->message_event_flag)
            {
                if (message->message_id == UARTBL_EVENT_ID_ACTIVATING_IMAGE)
                {
                    updatersm_change_state(UPDATER_STATE_IMAGE_ACTIVATE_DELAY);
                }
                else if (message->message_id == UARTBL_EVENT_ID_FAILURE)
                {
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
            }
        }
        break;

        case UPDATER_STATE_IMAGE_ACTIVATE_DELAY:
        {
            if (enter)
            {
                updatersm_schedule_timing_event(3000);
            }
            else if (timing_event)
            {
                wf_gem_hci_manager_bootloader_on_update_progress_changed(WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_UPDATE_COMPLETE, 0);
                updatersm_change_state(UPDATER_STATE_NULL);
            }
            else if (message && message->message_event_flag)
            {
                if (message->message_id == UARTBL_EVENT_ID_FAILURE)
                {
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
            }
        }
        break;

        case UPDATER_STATE_FAILURE:
        {
            if (enter)
            {
                wf_gem_hci_manager_bootloader_on_update_progress_changed(WF_GEM_HCI_MANAGER_BOOTLOADER_UPDATER_STATE_UPDATE_FAILED, 0);
            }
        }
        break;

        case UPDATER_STATE_EXIT_BL:
        {
            if (enter)
            {
                send_cmd_exit_bootloader(5, 100);
            }
            else if (timing_event)
            {
            }
            else if (message && !message->message_event_flag)
            {
                // this is a response to the command we sent
                updatersm_change_state(UPDATER_STATE_NULL);
            }
            else if (message && message->message_event_flag)
            {
                if (message->message_id == UARTBL_EVENT_ID_FAILURE)
                {
                    updatersm_change_state(UPDATER_STATE_FAILURE);
                }
            }
            else if (command_timeout)
            {
                // no response... transition to null state now
                updatersm_change_state(UPDATER_STATE_NULL);
            }
        }
        break;

    }
}


#endif // #if WF_GEM_HCI_CONFIG_INCLUDE_BOOTLOADER_MANAGER
